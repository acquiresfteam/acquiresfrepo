trigger AccountProfile on Account (before insert, before update) {

   	List<Id> accOwnerIds = new List<Id>();
    
    for(Account trigAcc: trigger.new)
    {
        accOwnerIds.add(trigAcc.OwnerId);
    }
    
    //Map<id,user> mpusers=new Map<id,user>([select id, name, profile.name from user where id in :accOwnerIds]);
      Map<id,user> mpusers = QueryUtility.getUserByListIds(accOwnerIds);
         
    for(Account trigAcc: trigger.new)
    {
      
        String newOwnerProfile = (mpusers.get(trigAcc.ownerid).profile.name).trim();
        
        if (Trigger.isUpdate)
        {
            if (trigger.old[0].OwnerId != trigAcc.OwnerId) {
                trigAcc.Owner_Profile__c = newOwnerProfile;
            }
        } else if (Trigger.isInsert) {
            trigAcc.Owner_Profile__c = newOwnerProfile;
        }
    }
}