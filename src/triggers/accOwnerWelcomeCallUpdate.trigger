trigger accOwnerWelcomeCallUpdate on Account (before update) {
    
    try 
    {
        List<Id> accOwnerIds = new List<Id>();
        for(Account acc: trigger.newMap.values())
        {
            accOwnerIds.add(acc.ownerid);
        }
           
        // Get users owning updated records
        //Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id in :accOwnerIds]);
           Map<id,user> mpuser = QueryUtility.getUserByListIds(accOwnerIds);
         
        
        //Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id =:trigger.newMap.OwnerId]);
        
        List<Event> welcomeCalls = new List<Event>([SELECT Id, AccountId, OwnerId, StartDateTime, Subject FROM Event WHERE AccountId =: trigger.old[0].Id AND Subject LIKE '%Welcome Call Booking%']);
        
        for(Account newAcc: trigger.newMap.Values()){
    
            String newOwnerProfile;
            
            // Get owner profile name
            if (mpuser.get(trigger.newMap.get(newAcc.id).ownerid).profile.name != NULL) {
                newOwnerProfile = (mpuser.get(trigger.newMap.get(newAcc.id).ownerid).profile.name).trim();
            }
            System.Debug('NewOwnerProfile found: ' + newOwnerProfile);
            // Check if account owner has changed and is a Champion or Edvisor, and has welcome calls
            if (trigger.old[0].OwnerId != trigger.new[0].OwnerId && 
                (newOwnerProfile == 'Acquire Career Champion' || newOwnerProfile == 'Eddi Edvisor')) {
            
                    System.Debug('Change of owner detected');
                    // Get Welcome Call events and reassign to new Account owner
                    System.Debug('Number of welcome calls found: ' + welcomeCalls.size());
                    for (Event wc : welcomeCalls) {
                        if (wc.OwnerId != trigger.new[0].OwnerId && wc.StartDateTime >= System.now()) {
                        wc.OwnerId = trigger.new[0].OwnerId;
                        System.Debug('Welcome call assigned to new account owner');
                    }
                }
            
                        
                } else {System.Debug('No change to Champ or Edvisor or welcome call found.');}
        }
        update welcomeCalls;
        
    } catch(exception ex){
            system.debug('ex ############:' + ex);
    }
    
}