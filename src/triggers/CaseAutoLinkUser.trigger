trigger CaseAutoLinkUser on Case (before insert, before update) 
{
  //Identify applicable record type
  RecordType sfSupport = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'Internal SF Support' limit 1];
  
  map<String, User> mpSuppEmail = new map<String, User>();
  set<String> strSuppEmail = new set<String>();

  for(Case c: trigger.new)
      strSuppEmail.add(c.SuppliedEmail);

  for(User u: [Select Email, Profile.Name from User where Email IN : strSuppEmail])
      mpSuppEmail.put(u.Email, u);

  system.debug(mpSuppEmail);

  if(!mpSuppEmail.values().isEmpty())
  {
    //Trigger where User Effected is blank and Supplied Email is populated
    User tmpUser;
    for (Case caseObj:Trigger.new) 
    {
        system.debug(caseObj.User_Effected__c);
        if (caseObj.User_Effected__c==null && !String.isBlank(caseObj.SuppliedEmail) && caseObj.RecordTypeId == sfSupport.Id)
        {
            tmpUser = mpSuppEmail.get(caseObj.SuppliedEmail);
            caseObj.User_Effected__c = tmpUser.Id;
            caseObj.Salesforce_Profile__c = tmpUser.Profile.Name;
        }
    }
  }
}