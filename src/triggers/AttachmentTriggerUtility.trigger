trigger AttachmentTriggerUtility on Attachment (after insert, before delete) {
    
    //this handler method is used to count number of attachments in Lead object
    NotesAndAttachmentTriggerHandler.CountAttachementsOnLead(trigger.new, trigger.old, trigger.isInsert, trigger.isDelete);
        
}