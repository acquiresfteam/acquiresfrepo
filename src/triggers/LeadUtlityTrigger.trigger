trigger LeadUtlityTrigger on Lead (before insert, before update) {
    if(trigger.isBefore){
       if(trigger.isUpdate){
            LeadTriggerHandler.NoConversionOnNewAndUnableToCallStatus(trigger.oldMap, trigger.newMap);
         }
        
        if(trigger.isUpdate || trigger.isInsert) {
         LeadTriggerHandler.updateLeadRating(trigger.oldMap, trigger.new, trigger.isInsert, trigger.isUpdate);
            
            LeadTriggerHandler.UpdateEmpolyerLeadRating(trigger.new);
        }
    }
}