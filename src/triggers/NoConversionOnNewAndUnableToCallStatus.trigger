trigger NoConversionOnNewAndUnableToCallStatus on Lead (before update){

    for(Lead leadrec : Trigger.new) {
    
        if(Trigger.OldMap.get(leadrec.Id).IsConverted != Trigger.newMap.get(leadrec.Id).IsConverted)
        {
            if(Trigger.OldMap.get(leadrec.Id).Status == 'New'||Trigger.OldMap.get(leadrec.Id).Status == 'Unable to Call')
                leadrec.addError('Conversion allowed only for leads with status as "Qualified for contact"');

        }
    
    }

}