trigger AttachmentTrigger on Attachment (after insert, after update) {
	if(Trigger.isAfter) {
		if(Trigger.isInsert || Trigger.isUpdate) {
			if(!attachmentUtil.disableTrigger) {
				attachmentUtil.prefixLeadAttachments(Trigger.new);
			}
		}
	}
} 