trigger countCV on Attachment (after insert, after update) {

    Set<String> parentIDs = new Set<String>();

    //get record IDs related to CV attachments
    for (Attachment at : Trigger.new) {
        if (at.Name.startsWithIgnoreCase('cv')) {
            parentIDs.add(at.parentid);
            System.Debug('CV attachment found');
        }
    }
 
    //get accounts related to attachments
    List<Account> accs = [SELECT Id, CV_attached__c FROM Account WHERE Id =: parentIDs];
    
    //update related account CV attached scheduled field to true
    for (Account a : accs) {
        if (a.CV_attached__c == FALSE) {
          a.CV_attached__c = TRUE;
      System.Debug('CV attached set to true');
        }
    }

    update accs;

    
}