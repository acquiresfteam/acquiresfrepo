/*
Update Trigger on Lead after conversion to Person Account only: 
a. Create Lead_Attachment__c object for each Attachment on Account object after Lead conversion
b. Create an attachment for the Lead_Attachment__c created
c. Delete the Attachment from Account object
@author: SMendu 
@CreatedDate: 28/04/2015
*/
trigger LeadConvertTrigger on Lead (after update) {
	
	List<Account> accountRes = new List<Account>();
	
	//Check if it is a Person Account
	if (trigger.new[0].ConvertedAccountId!=null)
		accountRes = [Select Id from Account where Id = :trigger.new[0].ConvertedAccountId and IsPersonAccount = true];
	
	//Lead is converted
	if((trigger.new[0].isConverted ==true && trigger.old[0].isConverted ==false) && accountRes.size()>0)
  	{	
			//Query for  Attachments from the converted Lead 
			List<Attachment> attchResultList = [Select Id, parentId, body,name,description from Attachment where ParentId = :Trigger.new[0].ConvertedAccountId];
			List<Attachment> insertAttchList = new List<Attachment>();
			List<Lead_Attachment__c> insertLeadAttchList = new List<Lead_Attachment__c>();
			List<Lead_Attachment__c> updateLeadAttchList = new List<Lead_Attachment__c>(); 
			Set<Id> setAttchIDs = new Set<Id>();
			Set<Id> setLeadAttchIDs = new Set<Id>();
			
			for (Attachment attchment : attchResultList){
				Lead_Attachment__c leadAttchment = new Lead_Attachment__c();
				leadAttchment.AcctParentId__c = attchment.ParentId;
				insertLeadAttchList.add(leadAttchment);
			}
			
			List<Database.SaveResult> leadAttchSaveRes = Database.insert(insertLeadAttchList, false);
		
			for(Integer i=0;i<leadAttchSaveRes.size();i++)
			{
				setLeadAttchIDs.add(leadAttchSaveRes[i].getId());
			}
			
			for(Integer i=0;i<leadAttchSaveRes.size();i++){
					Attachment newAttch = new Attachment();
	        		newAttch.body = attchResultList[i].body;
	        		newAttch.name = attchResultList[i].name;
	        		newAttch.description = attchResultList[i].description;
	        		newAttch.parentId = leadAttchSaveRes[i].getId();
	        		insertAttchList.add(newAttch);
	 		}
			
			List<Database.SaveResult> attchSaveRes = Database.insert(insertAttchList, false);
			
			List<Lead_Attachment__c> leadAttchQueRes = [select id,name, FileName__c,AttachmentId__c from Lead_Attachment__c where Id in :setLeadAttchIDs];
			
			List<Attachment> attchmentsList = [Select Id, body, name, description,ParentId from Attachment where ParentId in :setLeadAttchIDs];
			
			for (Lead_Attachment__c leadAttchment : leadAttchQueRes)
			{
				for(Integer i =0;i<attchmentsList.size();i++)
				{
					if(leadAttchment.id == attchmentsList[i].parentId)
					{
						leadAttchment.FileName__c = attchmentsList[i].name;
        				leadAttchment.AttachmentId__c = attchmentsList[i].Id;
        				updateLeadAttchList.add(leadAttchment);
					}
				}
			}
 			Database.update(updateLeadAttchList, false);
 			//Delete Account related Attachments
 			Database.delete(attchResultList, false);
  	}
}