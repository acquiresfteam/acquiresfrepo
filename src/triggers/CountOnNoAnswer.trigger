trigger CountOnNoAnswer on Task (after insert) {
    
    Set<Id> LeadIDs = new Set<ID> () ; 
    
    For( Task objTask : Trigger.New ) {
         if(objTask.Subject == 'No Answer')
         LeadIDs.add(objTask.whoid ) ;
    }

    if( LeadIDs != null && LeadIDs.size() > 0 ){
        List<Lead> updateLeadList = [ Select Id ,LeadSource , RecordType.Name,Count__c   from Lead where ID IN : LeadIDs ] ; 
        List<Lead> temp = new  List<Lead>();
        List<Task> updateTaskCount = [Select ID , whoID  from Task where WhoID =:  LeadIDs ] ; 
        
        for(Lead leadid : updateLeadList){

          if(String.ValueOf(leadid.LeadSource)!= null && 
           (String.ValueOf(leadid.LeadSource).Contains('Inbound') || leadid.LeadSource == 'Carlton FC') || 
           leadid.RecordType.Name == 'Employer'){ 
          
           if(leadid.Count__c == Null){    
            leadid.Count__c =1;
           } 
           else {
            leadid.Count__c++;
           }
           temp.add(leadid);
        
           }
        }
         update temp;
    }
}