<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Acquire Users Citation Tasks</fullName>
        <actions>
            <name>Create_New_Citation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Acquire_Users__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Acquire_Users__c.Current_Total_Points__c</field>
            <operation>equals</operation>
            <value>3.25,5,6.75,8</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Create_New_Citation</fullName>
        <assignedTo>franklyn@acquirelearning.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Create New Citation</subject>
    </tasks>
</Workflow>
