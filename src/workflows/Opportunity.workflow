<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Census_Date_Update</fullName>
        <ccEmails>censusdates@acquirelearning.com.au</ccEmails>
        <description>Census Date Update</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Census_Trigger</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_career_hunter</fullName>
        <description>Email alert to career hunter</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_career_hunter</template>
    </alerts>
    <alerts>
        <fullName>Email_to_notify_the_Career_Advisor</fullName>
        <description>Email to notify the Career Advisor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_Career_Advisor</template>
    </alerts>
    <alerts>
        <fullName>Email_to_notify_the_Sales_Admin_Retention_user</fullName>
        <description>Email to notify the Sales Admin - Retention user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_Sales_Admin_Retention_user</template>
    </alerts>
    <alerts>
        <fullName>Opp_Cancellation_Requested_notification_to_Retention_Sales_Admin</fullName>
        <description>Opp Cancellation Requested notification to Retention Sales Admin</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegan.swain@acquirelearning.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cancellation_Requested_for_Sales_Admin_Retention</template>
    </alerts>
    <alerts>
        <fullName>Retention_Sales_Admin_follow_up_of_deferred_student_on_date_deferred_to</fullName>
        <description>Retention Sales Admin follow up of deferred student on date deferred to</description>
        <protected>false</protected>
        <recipients>
            <recipient>tegan.swain@acquirelearning.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforce@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Deferral_end_date_alert</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email</fullName>
        <description>Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>syed.ahmed@acquirelearning.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Enrolment_Templates/Html_Example</template>
    </alerts>
    <alerts>
        <fullName>email_to_notify_the_Retention_Specialist</fullName>
        <description>email to notify the Retention Specialist</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_Retention_Specialist</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Stage_Update</fullName>
        <field>Opportunity_Stage__c</field>
        <literalValue>Course in Progress</literalValue>
        <name>Account Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancellation_Confirmed_Date</fullName>
        <field>Cancellation_Confirmed_Date__c</field>
        <formula>Today()</formula>
        <name>Cancellation Confirmed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancellation_Requested_Date</fullName>
        <field>Cancellation_Requested_Date__c</field>
        <formula>TODAY()</formula>
        <name>Cancellation Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Career_Advisor_ID</fullName>
        <field>Career_Advisor_ID__c</field>
        <formula>Owner.Id</formula>
        <name>Capture Career Advisor ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Career_Advisor_Name</fullName>
        <field>Career_Advisor_Name__c</field>
        <formula>Owner.FirstName &amp; &apos; &apos; &amp; Owner.LastName</formula>
        <name>Capture Career Advisor Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Confirmed_Enrolled_Date</fullName>
        <field>Confirmed_Enrolled_Date__c</field>
        <formula>Today()</formula>
        <name>Confirmed Enrolled Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Deferred_Date</fullName>
        <field>Course_Deffered_Confirmed_Date__c</field>
        <formula>Today()</formula>
        <name>Course Deferred Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Name_Update_Opp</fullName>
        <field>Course_Name__c</field>
        <formula>Course_LookUp__r.Name</formula>
        <name>Course Name Update Opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Departure_City_clear_update</fullName>
        <field>Departure_City__c</field>
        <name>Departure City clear update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Education_Provider_Name_update_Opps</fullName>
        <description>Update &apos;Education Provider Name&apos; field with the name of the &apos;Education Provider Account&apos; lookup</description>
        <field>Education_Provider_Name__c</field>
        <formula>Education_Provider_Account__r.Name</formula>
        <name>Education Provider Name update Opps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Forms_Completed_Date_Update</fullName>
        <field>Forms_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Forms Completed Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lost_Date</fullName>
        <field>Lost_Date__c</field>
        <formula>Today()</formula>
        <name>Lost Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_To_Employer</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Employer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp To Employer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_To_Other</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Default</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp To Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recent_opportunity_ID</fullName>
        <field>Recent_Closed_Opportunity_Id__c</field>
        <formula>Id</formula>
        <name>Recent opportunity ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_opp_stage_change_date</fullName>
        <field>Opp_Stage_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Record opp stage change date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Career_Advisor_ID</fullName>
        <field>Career_Advisor_ID__c</field>
        <name>Remove Career Advisor ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Career_Hunter_Owner_ID</fullName>
        <field>Career_Hunter_Owner_ID__c</field>
        <name>Remove Career Hunter Owner ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_commission_amount_when_not_confir</fullName>
        <description>Removes Commission amount on the field when the stage is not enrolled confirmed</description>
        <field>Career_Advisor_Commission__c</field>
        <name>Remove commission amount when not confir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Saved_Date</fullName>
        <field>Saved_Date__c</field>
        <formula>Today()</formula>
        <name>Saved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Course_Start_Date_to_Close_Date</fullName>
        <description>Map opp Close Date value to Course Start Date - currently only for Eddi opps</description>
        <field>Course_Start_Date__c</field>
        <formula>CloseDate</formula>
        <name>Set Course Start Date to Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_ID_Number</fullName>
        <description>Populate Opportunity ID field with unique ID formed by [record type abbr]+[autonumber]</description>
        <field>Opportunity_ID__c</field>
        <formula>CASE($RecordType.Name, 
&quot;Default&quot;, &quot;ENR&quot;&amp;ID_num__c, 
&quot;Employer&quot;, &quot;JOB&quot;&amp;ID_num__c,
NULL)</formula>
        <name>Set Opportunity ID Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Owner_to_Sales_Admin</fullName>
        <description>Update Account Owner to Sales Admin</description>
        <field>OwnerId</field>
        <lookupValue>marisa.rodriguez@acquirecareers.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Account Owner to Sales Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Career_Hunter_owner_Id</fullName>
        <field>Career_Hunter_Owner_ID__c</field>
        <formula>Owner.Id</formula>
        <name>Update Career Hunter owner Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Career_Shortlister_Owner_Name</fullName>
        <description>Update the shortlister&apos;s name when assigned by the recruitment sales admin</description>
        <field>Career_Shortlister_Owner_Name__c</field>
        <formula>Owner.FirstName &amp; &apos; &apos; &amp; Owner.LastName</formula>
        <name>Update Career Shortlister Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Career_Stage_on_Account</fullName>
        <field>Opportunity_Stage__c</field>
        <literalValue>Course in Progress</literalValue>
        <name>Update Career Stage on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date</fullName>
        <field>Won_Date__c</field>
        <formula>Today()</formula>
        <name>Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_When_Job_Is_Lost_Field</fullName>
        <description>Updates the field with the date of marking an opp as &quot;Job Lost&quot;</description>
        <field>Job_Lost_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Date When Job Is Lost Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_When_Job_Is_Shortlisted</fullName>
        <field>Job_Shortlisted_Date__c</field>
        <formula>Today()</formula>
        <name>Update Date When Job Is Shortlisted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_When_Job_Is_Won_Field</fullName>
        <description>Updates the field with the date of marking an opp as &quot;Job Won&quot;</description>
        <field>Job_Won_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Date When Job Is Won Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Enrolment_date_on_account1</fullName>
        <field>Rising_Star_Enrolment_Date__c</field>
        <formula>CloseDate</formula>
        <name>Update Enrolment date on account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_Placed_date_field</fullName>
        <description>The field updates the Job Placed date field. This field is used for reporting</description>
        <field>Job_Placed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Job Placed date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Owner_for_Canc_or_CD_Req</fullName>
        <field>OwnerId</field>
        <lookupValue>tegan.swain@acquirelearning.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Opp Owner for Canc or CD Req</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Owner_stage_Job_won</fullName>
        <field>OwnerId</field>
        <lookupValue>natalie@recruiteasy.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Opp Owner for stage Job won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_career_hunter_owner_Name</fullName>
        <field>Career_Hunter_Owner_Name__c</field>
        <formula>Owner.FirstName &amp; &apos; &apos; &amp; Owner.LastName</formula>
        <name>Update career hunter owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_commission_amount</fullName>
        <description>Updates the commission amount field with the commission amount value on a course</description>
        <field>Career_Advisor_Commission__c</field>
        <formula>Course_LookUp__r.Course_Commission_Amount__c</formula>
        <name>Update commission amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_course_on_account</fullName>
        <field>Course__c</field>
        <formula>Text(Course__c)</formula>
        <name>Update course on account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_education_provider_on_account</fullName>
        <field>Education_Provider__c</field>
        <formula>Text(Education_Provider__c)</formula>
        <name>Update education provider on account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_opp_owner_for_stage_and_age</fullName>
        <field>OwnerId</field>
        <lookupValue>tegan.swain@acquirelearning.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update opp owner for stage and age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_stage_to_Job_Placed</fullName>
        <description>Updates the stage to &apos;Job Placed&apos;</description>
        <field>StageName</field>
        <literalValue>Job Placed</literalValue>
        <name>Update stage to Job Placed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_field_with_current_date</fullName>
        <description>Assignment date update</description>
        <field>Job_assignment_date__c</field>
        <formula>TODAY()</formula>
        <name>Updates field with current date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Venture_Gold_Coast_Update</fullName>
        <field>Venture__c</field>
        <literalValue>Venture Gold Coast</literalValue>
        <name>Venture Gold Coast Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Venture_Melbourne_Update</fullName>
        <field>Venture__c</field>
        <literalValue>Venture Melbourne</literalValue>
        <name>Venture Melbourne Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Venture_Sydney_update</fullName>
        <field>Venture__c</field>
        <literalValue>Venture Sydney</literalValue>
        <name>Venture Sydney update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Venture_clear_update</fullName>
        <field>Venture__c</field>
        <name>Venture clear update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Verified_by_field_update</fullName>
        <description>Update this field with the name of the user</description>
        <field>Verified_By__c</field>
        <formula>$User.FirstName  &amp; &apos;, &apos; &amp;  $User.LastName</formula>
        <name>Verified by field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cancellation Confirmed Date</fullName>
        <actions>
            <name>Cancellation_Confirmed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 
ISPICKVAL(StageName, &quot;Cancellation confirmed&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation Requested Date</fullName>
        <actions>
            <name>Cancellation_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
ISPICKVAL(PRIORVALUE(StageName), &quot;Enrolled - Confirmed&quot;), 
ISPICKVAL(StageName,&quot;Cancellation Requested&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cancellation Requested from enroll confirmed or forms completed</fullName>
        <actions>
            <name>Opp_Cancellation_Requested_notification_to_Retention_Sales_Admin</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Temp workflow rule to catch cancel requested opps</description>
        <formula>AND( 
OR(ISPICKVAL(PRIORVALUE(StageName), &quot;Enrolled - Confirmed&quot;), ISPICKVAL(PRIORVALUE(StageName), &quot;Enrolled - Forms Completed&quot;)),
ISPICKVAL(StageName,&quot;Cancellation Requested&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Census Date Update</fullName>
        <actions>
            <name>Census_Date_Update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <description>Send Email to team to update census date when stage = Enrolled - Confirmed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Confirmed Enrolled Date</fullName>
        <actions>
            <name>Confirmed_Enrolled_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Corse Deffered Date</fullName>
        <actions>
            <name>Course_Deferred_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Course Deffered Confirmed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Course Name Update Opp</fullName>
        <actions>
            <name>Course_Name_Update_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the course name with the value in the Related Course field.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Education Provider Name Rule for Partners Opps</fullName>
        <actions>
            <name>Education_Provider_Name_update_Opps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update &apos;Education Provider Name&apos; field with the name of the Education Provider Account.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Forms Completed Date</fullName>
        <actions>
            <name>Forms_Completed_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
ISPICKVAL(PRIORVALUE(StageName), &quot;Contacted - Info Sent&quot;), 
ISPICKVAL(StageName,&quot;Enrolled - Forms Completed&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Job Placed when remaining positions equals zero</fullName>
        <actions>
            <name>Update_stage_to_Job_Placed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Remaining_Positions__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Number_of_positions__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>The opportunity stage becomes &apos;Job Placed&apos; when the number of remaining positions for a job equals zero.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead To Opp Emp Rec Type Copy</fullName>
        <actions>
            <name>Opp_To_Employer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Record_Type_Opp_Long__c</field>
            <operation>equals</operation>
            <value>Employer</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Acquire Business Builder</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead To Opp Other Record Type</fullName>
        <actions>
            <name>Opp_To_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Record_Type_Opp_Long__c</field>
            <operation>equals</operation>
            <value>Career Adviser</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Record_Type_Opp_Long__c</field>
            <operation>equals</operation>
            <value>Data Analyst</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Record_Type_Opp_Long__c</field>
            <operation>equals</operation>
            <value>Sales Admin</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Acquire Business Builder</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lost Date</fullName>
        <actions>
            <name>Lost_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Name of the user for student verification</fullName>
        <actions>
            <name>Verified_by_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Venture_Student_Verified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the name of the user</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Owner change notification</fullName>
        <actions>
            <name>email_to_notify_the_Retention_Specialist</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Owner.Profile.Name == &quot;Acquire Retention Specialist&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity created</fullName>
        <actions>
            <name>Set_Opportunity_ID_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity owner returns back to the Career Advisor</fullName>
        <actions>
            <name>Email_to_notify_the_Career_Advisor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( OR (  ISPICKVAL(PRIORVALUE(StageName), &apos;Cancellation Requested&apos;),  ISPICKVAL(PRIORVALUE(StageName), &apos;Course Deffered Requested&apos;)  ), OR (  ISPICKVAL(StageName , &apos;Contacted - Info Sent&apos;),  ISPICKVAL(StageName , &apos;Enrolled - Forms Completed&apos;)  ), $Profile.Name == &apos;Acquire Career Advisor&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Owner change notification to career hunter</fullName>
        <actions>
            <name>Email_alert_to_career_hunter</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Remove_Career_Hunter_Owner_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  Owner.Profile.Name == &apos;Acquire Career Hunter&apos;,  ISPICKVAL( StageName,&apos;Job Shortlisted&apos;),  NOT(ISBLANK(  Career_Hunter_Owner_ID__c ) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record opp stage change date</fullName>
        <actions>
            <name>Record_opp_stage_change_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( StageName )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove commission amount when not confirmed</fullName>
        <actions>
            <name>Remove_commission_amount_when_not_confir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <description>The rule updates the Commission amount field to null when an opportunity in not enrolled confirmed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Saved Date</fullName>
        <actions>
            <name>Saved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
ISPICKVAL(PRIORVALUE(StageName), &quot;Cancellation Requested&quot;), 
OR ( 
ISPICKVAL(StageName,&quot;Enrolled - Confirmed&quot;), 
ISPICKVAL(StageName,&quot;Contacted - Info Sent&quot;), 
ISPICKVAL(StageName,&quot;Enrolled - Forms Completed&quot;) 
))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Eddi Course Start Date</fullName>
        <actions>
            <name>Set_Course_Start_Date_to_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Education_Provider_Name__c</field>
            <operation>contains</operation>
            <value>Eddi</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Education_Provider_Name__c</field>
            <operation>notContain</operation>
            <value>Non Eddi</value>
        </criteriaItems>
        <description>DEACTIVATED DUE TO COURSE DATE SYSTEM ISSUE (PC):Set date to opp close date at Forms Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Shortlister Name Assignment Rule</fullName>
        <actions>
            <name>Update_Career_Shortlister_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updates_field_with_current_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&apos;Career Shortlister Name&apos; field gets updated as soon as employer opportunity is assigned to a Career Shortlister</description>
        <formula>AND (CONTAINS(Owner.UserRole.Name, &quot;Career Qualifiers&quot;),
 RecordType.Name = &quot;Employer&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Student deferred to date TBWF</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Course Deffered Confirmed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Date_Deferred_To__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Retention_Sales_Admin_follow_up_of_deferred_student_on_date_deferred_to</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Date_Deferred_To__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Account Owner to Sales Admin</fullName>
        <actions>
            <name>Update_Career_Stage_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <description>Used to re-assign accounts having Won opportunities to a Sales Admin queue for assignment to Career Champions</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Career Advisor Name and ID</fullName>
        <actions>
            <name>Email_to_notify_the_Sales_Admin_Retention_user</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Capture_Career_Advisor_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Capture_Career_Advisor_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opp_Owner_for_Canc_or_CD_Req</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates C.A. name and ID when C.A changes opp stage to canc. req. or course def req.</description>
        <formula>AND (   ISCHANGED(StageName),  OR (    ISPICKVAL(PRIORVALUE(StageName), &apos;Contacted - Info Sent&apos;),    ISPICKVAL(PRIORVALUE(StageName), &apos;Enrolled - Forms Completed&apos;)  ),  OR (    ISPICKVAL(StageName , &apos;Cancellation Requested&apos;),    ISPICKVAL(StageName , &apos;Course Deffered Requested&apos;)  ),   $Profile.Name == &apos;Acquire Career Advisor&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date When Job Is Lost</fullName>
        <actions>
            <name>Update_Date_When_Job_Is_Lost_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Job Lost</value>
        </criteriaItems>
        <description>Runs for employer opportunities, when the stage of the opp is &quot;Job Lost&quot;. Used for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Date When Job Is Placed</fullName>
        <actions>
            <name>Update_Job_Placed_date_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Job Placed</value>
        </criteriaItems>
        <description>Runs for employer opportunities, when the stage of the opp is &quot;Job Placed&quot;. Used for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Date When Job Is Won</fullName>
        <actions>
            <name>Update_Date_When_Job_Is_Won_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Job Won</value>
        </criteriaItems>
        <description>Runs for employer opportunities, when the stage of the opp is &quot;Job Won&quot;. Used for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Date when Job is Shortlisted</fullName>
        <actions>
            <name>Update_Date_When_Job_Is_Shortlisted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Job Shortlisted</value>
        </criteriaItems>
        <description>Job Shortlisted Date field is updated when job stage is changed to &apos;Job Shortlisted&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Enrolment date%2C edu provider and course on account</fullName>
        <actions>
            <name>Recent_opportunity_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Enrolment_date_on_account1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opp Owner for stage Job won</fullName>
        <actions>
            <name>Update_Career_Hunter_owner_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opp_Owner_stage_Job_won</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_career_hunter_owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND 
( 
OR 
( 
$Profile.Name == &apos;Acquire Career Hunter&apos;,$Profile.Name == &apos;Acquire Business Builder&apos;), 
OR 
( 
ISPICKVAL(StageName , &apos;Job Won&apos;),ISPICKVAL(StageName , &apos;Request for Reshortlisting&apos;) 
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update commission amount</fullName>
        <actions>
            <name>Update_commission_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <description>The rule updates the Commission amount field when an opportunity becomes enrolled confirmed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update opp owner for stage and 14 days age</fullName>
        <actions>
            <name>Update_opp_owner_for_stage_and_age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Age__c</field>
            <operation>greaterThan</operation>
            <value>14</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Contacted - Info Sent,Enrolled - Forms Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Employer</value>
        </criteriaItems>
        <description>All opps with stage as Contacted info sent  and Enrolled forms complete and age &gt; 14days must be assigned to Tegan Swain.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Venture Gold Coast Rule</fullName>
        <actions>
            <name>Departure_City_clear_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Venture_Gold_Coast_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS( Course_LookUp__r.Name, &quot;Venture Gold Coast&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Venture Melbourne Rule</fullName>
        <actions>
            <name>Departure_City_clear_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Venture_Melbourne_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS( Course_LookUp__r.Name, &quot;Venture Melbourne&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Venture Sydney rule</fullName>
        <actions>
            <name>Departure_City_clear_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Venture_Sydney_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS( Course_LookUp__r.Name , &quot;Venture Sydney&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Venture clear</fullName>
        <actions>
            <name>Venture_clear_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(  OR(    CONTAINS(Course_LookUp__r.Name, &quot;Venture Sydney&quot;),    CONTAINS(Course_LookUp__r.Name, &quot;Venture Melbourne&quot;),    CONTAINS(Course_LookUp__r.Name, &quot;Venture Gold Coast&quot;)    ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome email</fullName>
        <actions>
            <name>Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Won Date</fullName>
        <actions>
            <name>Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Enrolled - Confirmed</value>
        </criteriaItems>
        <description>Date when Opp is won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
