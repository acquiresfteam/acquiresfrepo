<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AIPT_Enrollment_Notification</fullName>
        <ccEmails>acquire.enrolments@aipt.edu.au</ccEmails>
        <ccEmails>aiptenrolments@acquirelearning.com.au</ccEmails>
        <description>AIPT Enrollment Notification</description>
        <protected>false</protected>
        <senderAddress>aiptenrolments@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Enrolment_Templates/AIPT_Enrollment_Submission</template>
    </alerts>
    <alerts>
        <fullName>APTI_VFH</fullName>
        <ccEmails>training@apti.edu.au</ccEmails>
        <description>APTI - VFH</description>
        <protected>false</protected>
        <senderAddress>apti.eddi@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/APTI_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>APTI_VFH_Acquire</fullName>
        <ccEmails>apti.eddi@acquirelearning.com.au</ccEmails>
        <description>APTI - VFH (Acquire)</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/APTI_Fee_Form_Help_Acquire</template>
    </alerts>
    <alerts>
        <fullName>Email_ACAE_Fee_Form_Help</fullName>
        <ccEmails>vocation@acquirelearning.com.au</ccEmails>
        <ccEmails>vetdata01@gmail.com</ccEmails>
        <description>Email ACAE Fee Form Help</description>
        <protected>false</protected>
        <senderAddress>vocation@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/ACAE_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMEDDIE_Enroll</fullName>
        <ccEmails>eddi.aim@acquirelearning.com.au</ccEmails>
        <ccEmails>pcst@aim.com.au</ccEmails>
        <description>Email AIMEDDIE Enroll</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIMEDDIE_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMEDDIE_Enroll_syed</fullName>
        <ccEmails>syed.moiz@acquirelearning.com.au</ccEmails>
        <description>Email AIMEDDIE Enroll(syed)</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AIMEDDIE_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMEDDIE_VETFee_Form</fullName>
        <ccEmails>pcst@aim.com.au</ccEmails>
        <description>Email AIMEDDIE-VETFee Form</description>
        <protected>false</protected>
        <senderAddress>aim@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIMEDDIE_VETFee_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMEDDIE_VETFee_Form_Acquire</fullName>
        <ccEmails>eddi.aim@acquirelearning.com.au</ccEmails>
        <description>Email AIMEDDIE-VETFee Form (Acquire)</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AIMEDDIE_VETFee_Form_Acquire</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMVET_Enroll</fullName>
        <ccEmails>aim@acquirelearning.com.au</ccEmails>
        <ccEmails>pcst@aim.com.au</ccEmails>
        <description>Email AIMVET Enroll</description>
        <protected>false</protected>
        <senderAddress>aim@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIMVET_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMVET_Fee</fullName>
        <ccEmails>pcst@aim.com.au</ccEmails>
        <description>Email AIMVET Fee</description>
        <protected>false</protected>
        <senderAddress>aim@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIMVET_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>Email_AIMVET_Fee_Acquire</fullName>
        <ccEmails>aim@acquirelearning.com.au</ccEmails>
        <description>Email AIMVET Fee (Acquire)</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIMVET_Fee_Form_Help_Acquire</template>
    </alerts>
    <alerts>
        <fullName>Email_AIM_Enroll</fullName>
        <ccEmails>aim@acquirelearning.com.au</ccEmails>
        <ccEmails>pct@aim.com.au</ccEmails>
        <description>Email - AIM Enroll</description>
        <protected>false</protected>
        <senderAddress>aim@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIM_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_AIM_Fee</fullName>
        <ccEmails>aim@acquirelearning.com.au</ccEmails>
        <ccEmails>pct@aim.com.au</ccEmails>
        <description>Email - AIM Fee</description>
        <protected>false</protected>
        <senderAddress>aim@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIM_Fee_Form_HTML</template>
    </alerts>
    <alerts>
        <fullName>Email_AIPE_Enroll</fullName>
        <ccEmails>info@studyonline.edu.au</ccEmails>
        <description>Email AIPE Enroll</description>
        <protected>false</protected>
        <senderAddress>aipe@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIPE_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_AIPE_Fee_Form_Help</fullName>
        <ccEmails>info@studyonline.edu.au</ccEmails>
        <description>Email - AIPE - Fee Form Help</description>
        <protected>false</protected>
        <senderAddress>aipe@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AIPE_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>Email_APTI_Enroll</fullName>
        <ccEmails>training@apti.edu.au</ccEmails>
        <description>Email  APTI Enroll</description>
        <protected>false</protected>
        <senderAddress>apti.eddi@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/APTI_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_Beauty_EDU</fullName>
        <ccEmails>grenadi@acquirelearning.com.au</ccEmails>
        <ccEmails>demandco@gmail.com</ccEmails>
        <ccEmails>shermoustafa@beautyedu.edu.au</ccEmails>
        <description>Email  Beauty EDU</description>
        <protected>false</protected>
        <senderAddress>grenadi@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Beauty_EDU_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_CFH_Enroll</fullName>
        <ccEmails>tcohaf@acquirelearning.com.au</ccEmails>
        <ccEmails>admin@thecollegeofhealthandfitness.qld.edu.au</ccEmails>
        <description>Email CFH Enroll</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CFH_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_CGU_Form</fullName>
        <ccEmails>cqu@acquirelearning.com.au</ccEmails>
        <ccEmails>online@train.cqu.edu.au</ccEmails>
        <description>Email CGU Form</description>
        <protected>false</protected>
        <senderAddress>cqu@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CQUni_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_Chifley</fullName>
        <ccEmails>mba-acquire@chifley.edu.au</ccEmails>
        <ccEmails>demandco@gmail.com</ccEmails>
        <description>Email Chifley</description>
        <protected>false</protected>
        <senderAddress>chifley@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Chifley_Fee_Form_HTML</template>
    </alerts>
    <alerts>
        <fullName>Email_Eddi</fullName>
        <ccEmails>edditest@acquirelearning.com.au</ccEmails>
        <description>Email  Eddi</description>
        <protected>false</protected>
        <senderAddress>edditest@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Eddi_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_FS_Enroll</fullName>
        <ccEmails>AcquireDiploma@franklynscholar.edu.au</ccEmails>
        <ccEmails>franklynscholar@acquirelearning.com.au</ccEmails>
        <description>Email - FS Enroll</description>
        <protected>false</protected>
        <senderAddress>franklynscholar@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Franklyn_Scholar_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_FS_Fee</fullName>
        <ccEmails>AcquireDiploma@franklynscholar.edu.au</ccEmails>
        <description>Email - FS Fee</description>
        <protected>false</protected>
        <senderAddress>franklynscholar@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Franklyn_Scholar_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>Email_FS_Fee_Acquire</fullName>
        <ccEmails>franklynscholar@acquirelearning.com.au</ccEmails>
        <description>Email - FS Fee (Acquire)</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Franklyn_Scholar_Fee_Form_Help_Acquire</template>
    </alerts>
    <alerts>
        <fullName>Email_Melpolytechnic_Enroll</fullName>
        <ccEmails>eddi.nmit@acquirelearning.com.au</ccEmails>
        <ccEmails>vetfeehelp@melbournepolytechnic.edu.au</ccEmails>
        <description>Email Melpolytechnic Enroll</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/MelPolytechnicEDDIE_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_NMITVET_Fee</fullName>
        <ccEmails>vetfeehelp@melbournepolytechnic.edu.au</ccEmails>
        <description>Email NMITVET Fee</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NMITVET_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>Email_NMITVET_Fee_Acquire</fullName>
        <ccEmails>eddi.nmit@acquirelearning.com.au</ccEmails>
        <description>Email NMITVET Fee(Acquire)</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/NMITVET_Fee_Form_Help_Acquire</template>
    </alerts>
    <alerts>
        <fullName>Email_Vocation_Form</fullName>
        <ccEmails>vocation@acquirelearning.com.au</ccEmails>
        <ccEmails>vetdata01@gmail.com</ccEmails>
        <description>Email Vocation Form</description>
        <protected>false</protected>
        <senderAddress>vocation@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Vocation_ACAE_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_t3_Enroll</fullName>
        <ccEmails>t3@acquirelearning.com.au</ccEmails>
        <ccEmails>admin@t3australia.com.au</ccEmails>
        <description>Email t3 Enroll</description>
        <protected>false</protected>
        <senderAddress>t3@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/T3_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>FS_EDDI_Enroll</fullName>
        <ccEmails>EDDIDiploma@franklynscholar.edu.au</ccEmails>
        <ccEmails>franklynscholar.eddi@acquirelearning.com.au</ccEmails>
        <description>FS - EDDI Enroll</description>
        <protected>false</protected>
        <senderAddress>franklynscholar@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Franklyn_Scholar_EDDI_Enrollment_Form</template>
    </alerts>
    <alerts>
        <fullName>FS_EDDI_Fee</fullName>
        <ccEmails>EDDIDiploma@franklynscholar.edu.au</ccEmails>
        <description>FS - EDDI Fee</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Franklyn_Scholar_EDDI_Fee_Form_Help</template>
    </alerts>
    <alerts>
        <fullName>FS_EDDI_Fee_Acquire</fullName>
        <ccEmails>franklynscholar.eddi@acquirelearning.com.au</ccEmails>
        <description>FS - EDDI Fee (Acquire)</description>
        <protected>false</protected>
        <senderAddress>philip.clark@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Franklyn_Scholar_EDDI_Fee_Form_Help_Acquire</template>
    </alerts>
    <alerts>
        <fullName>MEGT_Enrolment_Submission_Notification</fullName>
        <ccEmails>ellie_little@megt.com.au</ccEmails>
        <ccEmails>megt@acquirelearning.com.au</ccEmails>
        <ccEmails>lou_boast@megt.com.au</ccEmails>
        <description>MEGT Enrolment Submission Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>renata@acquirelearning.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>megt@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Enrolment_Templates/MEGT_Enrolment_Submission</template>
    </alerts>
    <alerts>
        <fullName>MEGT_VETFee_Notification</fullName>
        <ccEmails>ellie_little@megt.com.au</ccEmails>
        <ccEmails>megt@acquirelearning.com.au</ccEmails>
        <ccEmails>lou_boast@megt.com.au</ccEmails>
        <description>MEGT VET Fee Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>renata@acquirelearning.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>megt@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Enrolment_Templates/MEGT_VET_Fee_Submission</template>
    </alerts>
    <alerts>
        <fullName>T3_Web_Fee</fullName>
        <ccEmails>admin@t3australia.com.au</ccEmails>
        <ccEmails>t3@acquirelearning.com.au</ccEmails>
        <description>T3 - Web Fee</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/T3_Fee_Form_Help</template>
    </alerts>
    <fieldUpdates>
        <fullName>Acquire_Inbound_PhLead_Source_from_LSfCA</fullName>
        <description>If Lead is updated by Career Advisor and Lead Source for Career Advisor = Acquire Inbound Phone, set Lead Source to Acquire Inbound Phone</description>
        <field>LeadSource</field>
        <literalValue>Acquire Inbound Phone</literalValue>
        <name>Acquire Inbound PhLead Source from LSfCA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Lead_Created_Date</fullName>
        <field>Create_Date_Copy__c</field>
        <formula>CreatedDate</formula>
        <name>Copy Lead Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Removes_the_date_from_Actioned_Date</fullName>
        <description>Removes the date from &quot;Actioned Date&quot;, when transferred to the Unable to call queue</description>
        <field>Actioned_Date__c</field>
        <name>Removes the date from &quot;Actioned Date&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Removes_the_date_from_Assignment_Date</fullName>
        <description>Removes the date from the assignment date field</description>
        <field>Assignment_Date__c</field>
        <name>Removes the date from &quot;Assignment Date&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetActiveLeadFalse</fullName>
        <field>Active_Lead__c</field>
        <literalValue>0</literalValue>
        <name>SetActiveLeadFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetLeadStatus_to_UnabletoCall</fullName>
        <field>Status</field>
        <literalValue>Unable to Call</literalValue>
        <name>SetLeadStatus to Unable to Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetNoAnswerFalse</fullName>
        <field>No_Answer_Flag__c</field>
        <literalValue>0</literalValue>
        <name>SetNoAnswerFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetNoAnswerTrue</fullName>
        <description>Sets the flag No Answer to be True.</description>
        <field>No_Answer_Flag__c</field>
        <literalValue>1</literalValue>
        <name>SetNoAnswerTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetNoAnswerWFRule_cFalse</fullName>
        <description>Sets the field NoAnswerWFRule__c back to false</description>
        <field>NoAnswerWFRule__c</field>
        <literalValue>0</literalValue>
        <name>SetNoAnswerWFRule__cFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetVoicemailWFRule_cFalse</fullName>
        <description>Sets the field VoicemailWFRule__c back to false</description>
        <field>VoicemailWFRule__c</field>
        <literalValue>0</literalValue>
        <name>SetVoicemailWFRule__cFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assignment_Date</fullName>
        <description>Set Assignment Date</description>
        <field>Assignment_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Assignment Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Set Lead Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_to_QFC</fullName>
        <description>Set lead to QFC if New - currently only for Contactability, Open Top and Performance Factory lead sources</description>
        <field>Status</field>
        <literalValue>Qualified for Contact</literalValue>
        <name>Set Lead to QFC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_reason_to_Do_Not_Call</fullName>
        <field>Status_Reason__c</field>
        <literalValue>DO NOT CALL</literalValue>
        <name>Set status reason to Do Not Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Stage_Field4</fullName>
        <description>Acquire Concept Shop</description>
        <field>LeadSource</field>
        <literalValue>Acquire Concept Shop</literalValue>
        <name>Update Account Stage Field4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Stage_Field6</fullName>
        <description>Acquire Bus</description>
        <field>LeadSource</field>
        <literalValue>Acquire Bus</literalValue>
        <name>Update Account Stage Field6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Stage_Field7</fullName>
        <description>Acquire Kiosks</description>
        <field>LeadSource</field>
        <literalValue>Acquire Kiosks</literalValue>
        <name>Update Account Stage Field7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Stage_Field8</fullName>
        <description>Australia Post</description>
        <field>LeadSource</field>
        <literalValue>Australia Post</literalValue>
        <name>Update Account Stage Field8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date</fullName>
        <field>Date_Unable_to_Call__c</field>
        <formula>TODAY()</formula>
        <name>Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Education_Provider</fullName>
        <field>Education_Provider_Text__c</field>
        <formula>TEXT ( Owner:User.Education_Provider__c )</formula>
        <name>Update Education Provider</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Actioned_Career_Advisor</fullName>
        <description>Update lead actioned career advisor</description>
        <field>Actioned_Career_Advisor__c</field>
        <formula>$User.FirstName + &quot; &quot; + $User.LastName</formula>
        <name>Update Lead Actioned Career Advisor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Actioned_Date</fullName>
        <description>Update Lead Actioned Date</description>
        <field>Actioned_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Lead Actioned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Actioned_Status</fullName>
        <description>Update lead actioned status</description>
        <field>Actioned_Status__c</field>
        <formula>IF( 
ISPICKVAL(Status, &quot;Unable to Call&quot;), &quot;Unable to Call&quot;, 
IF(ISPICKVAL(Status, &quot;Closed&quot;), &quot;Closed&quot;, 
IF(ISPICKVAL(Status, &quot;Contacted&quot;), &quot;Contacted&quot;,&quot;&quot;)))</formula>
        <name>Update Lead Actioned Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Age_Employer_Field</fullName>
        <field>Create_Date_Employer__c</field>
        <formula>CreatedDate</formula>
        <name>Update Lead Age Employer Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Record_Type</fullName>
        <description>Updates Lead Record  Type to Advisor when transferred from PH team. This ensures the Career Advisors have the correct Lead Status Reasons.</description>
        <field>RecordTypeId</field>
        <lookupValue>Career_Adviser</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Record_Type_for_Spot_Jobs_Im</fullName>
        <description>Updates to Data Analyst Record Type when Spot Jobs import is performed.</description>
        <field>RecordTypeId</field>
        <lookupValue>Data_Analyst</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Lead Record Type for Spot Jobs Im</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Record_Type_to_Onshore</fullName>
        <description>Updates Lead Record Type to Onshore Leads coming in via web as D Dau owner.</description>
        <field>RecordTypeId</field>
        <lookupValue>Career_Adviser</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Lead Record Type to Onshore</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_Outbound_Referral</fullName>
        <description>Updates Lead Source to Outbound Referral if created by Career Advisor.</description>
        <field>LeadSource</field>
        <literalValue>Outbound Referral</literalValue>
        <name>Update Lead Source Outbound Referral</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field1</fullName>
        <description>Update &quot;Acquire Inbound Chat&quot;</description>
        <field>LeadSource</field>
        <literalValue>Acquire Inbound Chat</literalValue>
        <name>Update Lead Source field1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field10</fullName>
        <description>Carlton FC</description>
        <field>LeadSource</field>
        <literalValue>Carlton FC</literalValue>
        <name>Update Lead Source field10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field11</fullName>
        <field>LeadSource</field>
        <literalValue>ROKT</literalValue>
        <name>Update Lead Source field11</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field2</fullName>
        <description>Acquire Inbound Phone</description>
        <field>LeadSource</field>
        <literalValue>Acquire Inbound Phone</literalValue>
        <name>Update Lead Source field2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field3</fullName>
        <description>Acquire Inbound Web</description>
        <field>LeadSource</field>
        <literalValue>Acquire Inbound Web</literalValue>
        <name>Update Lead Source field3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field4</fullName>
        <description>Outbound Referral</description>
        <field>LeadSource</field>
        <literalValue>Outbound Referral</literalValue>
        <name>Update Lead Source field4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_field9</fullName>
        <description>Gloria Jeans Coffees</description>
        <field>LeadSource</field>
        <literalValue>Gloria Jeans Coffees</literalValue>
        <name>Update Lead Source field9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Source_for_Career_Advisor</fullName>
        <description>Updates the field value to &quot;OutBound Referral&apos;.</description>
        <field>Lead_Sources_for_Career_Advisor__c</field>
        <literalValue>Outbound Referral</literalValue>
        <name>Update Lead Source for Career Advisor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Nationality</fullName>
        <description>It will update the nationality status as Australian Citizen when the lead source is Contactibility, Performance Factory, Open Top</description>
        <field>Nationality_Status__c</field>
        <literalValue>Australian Citizen</literalValue>
        <name>Update Nationality</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Profile_DataAnalyst</fullName>
        <field>Profile_Data_Analyst__c</field>
        <formula>$User.FirstName + &quot; &quot; +  $User.LastName</formula>
        <name>Update Profile Data Analyst</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Profile_Date</fullName>
        <description>Update profile date to Today when lead is set to Qualified for Contact or Closed by Offshore</description>
        <field>Profile_Date__c</field>
        <formula>Today()</formula>
        <name>Update Profile Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Profile_Status</fullName>
        <description>Update profile status to capture the lead status when a lead has been profiled by Offshore</description>
        <field>Profile_Status__c</field>
        <formula>IF(ISPICKVAL(Status, &quot;Qualified for Contact&quot;), &quot;Qualified for Contact&quot;,
IF(ISPICKVAL(Status, &quot;Closed&quot;), &quot;Closed&quot;, &quot;&quot;))</formula>
        <name>Update Profile Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <description>The lead record type will be set as &quot;Acquire Career Advisor&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Career_Adviser</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Spot_Jobs_Record_Owner_Imported</fullName>
        <description>Upon Spot Jobs Lead Import, update Record Owner to PH Spot Jobs Unassigned.</description>
        <field>OwnerId</field>
        <lookupValue>PH_Spot_Jobs_Unassigned</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Spot Jobs Record Owner Imported</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_lead_source_with_Career_One_Emp</fullName>
        <field>LeadSource</field>
        <literalValue>Career One (Employer)</literalValue>
        <name>Update lead source with Career One (Emp)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Adzuna_to_lead_source</fullName>
        <field>LeadSource</field>
        <literalValue>Adzuna</literalValue>
        <name>Updates Adzuna to lead source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Employer_Referral_to_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>Employer Referral</literalValue>
        <name>Updates Employer Referral to Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Free_Job_Sits_to_leadsource</fullName>
        <field>LeadSource</field>
        <literalValue>Free Job Sits</literalValue>
        <name>Updates Free Job Sits to leadsource</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Gumtree_to_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>Gumtree</literalValue>
        <name>Updates Gumtree to Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Indeed_to_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>Indeed</literalValue>
        <name>Updates Indeed to Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_My_Career_to_Lead_source</fullName>
        <field>LeadSource</field>
        <literalValue>My Career</literalValue>
        <name>Updates My Career to Lead source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Seek_to_Lead_source</fullName>
        <field>LeadSource</field>
        <literalValue>Seek</literalValue>
        <name>Updates Seek to Lead source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_Spot_jobs_emp_to_lead_source</fullName>
        <field>LeadSource</field>
        <literalValue>Spot Jobs (Employer)</literalValue>
        <name>Updates Spot jobs (emp) to lead source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACAE - Fee Form Help</fullName>
        <actions>
            <name>Email_ACAE_Fee_Form_Help</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Vocation ACAE-Fee&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AIM - Enrollment</fullName>
        <actions>
            <name>Email_AIM_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIM-Enroll</value>
        </criteriaItems>
        <description>This workflow will send an email template of AIM enrollment form to AIM and Acquire email addresses.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AIM- VFH</fullName>
        <actions>
            <name>Email_AIM_Fee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIM-Fee</value>
        </criteriaItems>
        <description>This workflow will send an email template of AIM VFH form to AIM and Acquire email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AIMEDDIE Enrollment Notification</fullName>
        <actions>
            <name>Email_AIMEDDIE_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIMEDDIE-Enroll</value>
        </criteriaItems>
        <description>This workflow will send AIM enrollment form to AIM and eddi email address</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AIMEDDIE Enrollment Notification %28Syed%29</fullName>
        <actions>
            <name>Email_AIMEDDIE_Enroll_syed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND (OR(!ISBLANK(Residential_Address__c), !ISBLANK(Residential_Postcode__c),
!ISBLANK(TEXT(Residential_State__c)), !ISBLANK (Residential_Suburb__c )), 
OR (ISCHANGED(Residential_Address__c),ISCHANGED(Residential_Postcode__c), ISCHANGED(Residential_State__c), ISCHANGED(Residential_Suburb__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AIMEDDIE-VET Fee Notification</fullName>
        <actions>
            <name>Email_AIMEDDIE_VETFee_Form</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_AIMEDDIE_VETFee_Form_Acquire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIMEDDIE-VETFee</value>
        </criteriaItems>
        <description>This Workflow will send email alert of VFH form to AIM and eddi email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AIMVET Enrollment Notification</fullName>
        <actions>
            <name>Email_AIMVET_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIMVET-Enroll</value>
        </criteriaItems>
        <description>This workflow will send an email alert of VFH form to AIM and Acquire&apos;s email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AIMVET Fee Notification</fullName>
        <actions>
            <name>Email_AIMVET_Fee</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_AIMVET_Fee_Acquire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIMVET-Fee</value>
        </criteriaItems>
        <description>This workflow will send an email alert of AIM VFH form to AIM and acquire email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AIPE - Fee Form Help</fullName>
        <actions>
            <name>Email_AIPE_Fee_Form_Help</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIPE-Fee</value>
        </criteriaItems>
        <description>This workflow will send an email alert of AIPE help form to AIPE and Acquire email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APTI - Enrollment</fullName>
        <actions>
            <name>Email_APTI_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-APTI-Enroll</value>
        </criteriaItems>
        <description>This workflow will send an email template of APTI enrollment form to APTI and Acquire email addresses.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>APTI - VFH</fullName>
        <actions>
            <name>APTI_VFH</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>APTI_VFH_Acquire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-APTI-Fee</value>
        </criteriaItems>
        <description>This workflow will send an email template of APTI VFH form to APTI and Acquire&apos;s email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AU_Outbound_Leads_Nationality_Status</fullName>
        <actions>
            <name>Update_Nationality</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Contactability,Open Top,Performance Factory</value>
        </criteriaItems>
        <description>This rule will update the Nationality Status to &quot;Australian Citizen&quot; and set the &quot;Record Type&quot; as &quot;Career Advisor&quot;  when the lead source will be set as Contactability, Performance Factory, Open Top</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CFH Enrollment</fullName>
        <actions>
            <name>Email_CFH_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-CFH-Enroll</value>
        </criteriaItems>
        <description>The workflow will send CFH enrollment form to CFH and acquire&apos;s email addresses</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CreateNoAnswerTask</fullName>
        <actions>
            <name>SetActiveLeadFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetNoAnswerTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetNoAnswerWFRule_cFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>No_Answer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.NoAnswerWFRule__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow creates a No Answer task and then sets the field &quot;NoAnswerWFRule__c&quot; back to false.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CreateVoicemailTask</fullName>
        <actions>
            <name>SetActiveLeadFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetLeadStatus_to_UnabletoCall</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetVoicemailWFRule_cFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Voicemail</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.VoicemailWFRule__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow creates a voicemail task and then sets the field &quot;VoicemailWFRule__c&quot; back to false.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Contact</fullName>
        <actions>
            <name>Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.DoNotCall</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>CFC Reinvent your Game Day Comp,Reinvent Your Career Expo</value>
        </criteriaItems>
        <description>This rule will evaluate the &quot;Do Not Call&quot; field and and will set the Lead Status &quot;Closed&quot; if it is unchecked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Do not call Lead</fullName>
        <actions>
            <name>Set_Lead_Status_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_status_reason_to_Do_Not_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.DoNotCall</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email -  Beauty EDU</fullName>
        <actions>
            <name>Email_Beauty_EDU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Beauty EDU-Enroll</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email - Chifley Fee</fullName>
        <actions>
            <name>Email_Chifley</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Chifley-Fee</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email AIPE Enroll</fullName>
        <actions>
            <name>Email_AIPE_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-AIPE-Enroll</value>
        </criteriaItems>
        <description>This workflow will email the email alert  of AIPE enrollment form to AIPE and Acquire email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email CGU</fullName>
        <actions>
            <name>Email_CGU_Form</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-CQUni-Enroll</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Eddi</fullName>
        <actions>
            <name>Email_Eddi</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Eddi-Enroll</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email T3</fullName>
        <actions>
            <name>Email_t3_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-T3-Enroll</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email T3 Fee Form Help</fullName>
        <actions>
            <name>T3_Web_Fee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-T3-Fee</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Vocation</fullName>
        <actions>
            <name>Email_Vocation_Form</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Vocation ACAE-Enroll</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Franklyn Scholar %28EDDI%29 - Fee Form Help</fullName>
        <actions>
            <name>FS_EDDI_Fee</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FS_EDDI_Fee_Acquire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Franklyn Scholar-Fee(EDDI)</value>
        </criteriaItems>
        <description>This workflow will send Franklyn Scholar eddi fee form to Franklyn Scholar and eddi through email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Franklyn Scholar %28EDDI%29 Enrollment Form</fullName>
        <actions>
            <name>FS_EDDI_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Franklyn Scholar-Enroll (EDDI)</value>
        </criteriaItems>
        <description>This workflow will send an email template of Franklyn Scholar email template to Franklyn Scholar and eddi email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Franklyn Scholar - Enrollment</fullName>
        <actions>
            <name>Email_FS_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Franklyn Scholar-Enroll</value>
        </criteriaItems>
        <description>This workflow will send an email alert of Franklyn Scholar enrollment form to Franklyn Scholar and Acquire&apos;s email addresses.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Franklyn Scholar - Fee Form Help</fullName>
        <actions>
            <name>Email_FS_Fee</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_FS_Fee_Acquire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-Franklyn Scholar-Fee</value>
        </criteriaItems>
        <description>This workflow will send an email template of Franklyn Scholar enrollment form to Franklyn Scholar and Acquire email addresses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Created</fullName>
        <actions>
            <name>Copy_Lead_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Create_Date_Copy__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MEGT Enrolment Created</fullName>
        <actions>
            <name>MEGT_Enrolment_Submission_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Webform_Source__c</field>
            <operation>contains</operation>
            <value>megt</value>
        </criteriaItems>
        <description>Triggered when an MEGT Enrolment lead is created from their web2lead form.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MEGT VET Fee Lead Created</fullName>
        <actions>
            <name>MEGT_VETFee_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.VETFEE_Form_Url__c</field>
            <operation>contains</operation>
            <value>megt</value>
        </criteriaItems>
        <description>Triggered when an MEGT VET Fee lead is created from their web2lead form.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Match Acquire Inbound Phone Lead Sources if Advisor Updates</fullName>
        <actions>
            <name>Acquire_Inbound_PhLead_Source_from_LSfCA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Career Advisor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Inbound Phone</value>
        </criteriaItems>
        <description>Update Lead Source if Lead Source for Career Advisor is Acquire Inbound Phone</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Melbourne Polytechnic - Enrollment</fullName>
        <actions>
            <name>Email_Melpolytechnic_Enroll</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-NMITEDDIE-Enroll</value>
        </criteriaItems>
        <description>This Workflow is sending Enrollment forms through email to the Melbourne Polytechnic and eddi</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NMITVET Fee Notification</fullName>
        <actions>
            <name>Email_NMITVET_Fee</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_NMITVET_Fee_Acquire</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web-NMITVET-Fee</value>
        </criteriaItems>
        <description>This workflow will send an email alert of VFH forms to Melbourne Polytechnic and eddi email addresses</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Lead Source is Contactability Open Top or Performance Factory</fullName>
        <actions>
            <name>Set_Lead_to_QFC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Contactability,Open Top,Performance Factory</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Check for new Leads with Lead Source Contactability Open Top or Performance Factory</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Qualified for Contact - Set System Fields</fullName>
        <actions>
            <name>SetNoAnswerFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow performs clean up of some hidden fields if the lead is ever set to &quot;Qualified for Contact&quot;</description>
        <formula>ISCHANGED(Status) &amp;&amp; ISPICKVAL(Status,&quot;Qualified for Contact&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove Assignment date when transferred to Unable to call queue</fullName>
        <actions>
            <name>Removes_the_date_from_Actioned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Removes_the_date_from_Assignment_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>contains</operation>
            <value>Unable to Contact</value>
        </criteriaItems>
        <description>Removes the date value from the assignment date field when the lead is transferred to the Unable to contact queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Enrollment Data to AIPT Providers</fullName>
        <actions>
            <name>AIPT_Enrollment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Webform_Source_Display__c</field>
            <operation>equals</operation>
            <value>AIPT</value>
        </criteriaItems>
        <description>Sends email alerts for providers</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetActiveLeadFalse</fullName>
        <actions>
            <name>SetActiveLeadFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the status is changed on any lead this will set the active lead flag to false.</description>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Unable to call Date</fullName>
        <actions>
            <name>Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Unable to Call</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Actioned Fields when Career Advisor details</fullName>
        <actions>
            <name>Update_Lead_Actioned_Career_Advisor</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Actioned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Actioned_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Actioned Career Advisor, Actioned Date &amp; Actioned Status when Lead is actioned by Career Advisor.</description>
        <formula>AND(ISCHANGED( Status ),   OR(ISPICKVAL(Status, &quot;Closed&quot;),ISPICKVAL(Status, &quot;Contacted&quot;),ISPICKVAL(Status, &quot;Unable to Call&quot;)) ,   OR($Profile.Name = &quot;Acquire Career Advisor&quot;,$Profile.Name = &quot;Acquire Training Admin&quot;,$Profile.Name = &quot;Acquire Sales Admin&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source</fullName>
        <actions>
            <name>Updates_Seek_to_Lead_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Seek</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Seek&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source 8</fullName>
        <actions>
            <name>Updates_Employer_Referral_to_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Employer Referral</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Employer Referral&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source1</fullName>
        <actions>
            <name>Update_lead_source_with_Career_One_Emp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Career One (Employer)</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Career One (Employer)&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source2</fullName>
        <actions>
            <name>Updates_My_Career_to_Lead_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>My Career</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;My Career&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source3</fullName>
        <actions>
            <name>Updates_Indeed_to_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Indeed</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Indeed&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source4</fullName>
        <actions>
            <name>Updates_Spot_jobs_emp_to_lead_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Spot Jobs (Employer)</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Spot Jobs(Employer)&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source5</fullName>
        <actions>
            <name>Updates_Gumtree_to_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Gumtree</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Gumtree&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source6</fullName>
        <actions>
            <name>Updates_Free_Job_Sits_to_leadsource</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Free Job Site</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Free Job Site&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employer Lead Source7</fullName>
        <actions>
            <name>Updates_Adzuna_to_lead_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_for_Employer__c</field>
            <operation>equals</operation>
            <value>Adzuna</value>
        </criteriaItems>
        <description>Updates Employer Lead Source &quot;Adzuna&quot; will populate in the Lead source field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Age Employer</fullName>
        <actions>
            <name>Update_Lead_Age_Employer_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Create_Date_Employer__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Assignment Date</fullName>
        <actions>
            <name>Set_Assignment_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Lead Assignment Date when a lead is assigned from Queue to Career Advisor</description>
        <formula>AND(ISCHANGED( OwnerId ),   OR(Owner:User.Profile.Name = &quot;Acquire Career Advisor&quot;,Owner:User.Profile.Name = &quot;Acquire Sales Admin - Inbound&quot;) ,   ISBLANK( Actioned_Date__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Owner to PH Spot Jobs Queue</fullName>
        <actions>
            <name>Update_Spot_Jobs_Record_Owner_Imported</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Spot Jobs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Upon import of Spot Jobs data change Owner to PH Spot Jobs Unassigned.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for D Dau Leads</fullName>
        <actions>
            <name>Update_Lead_Record_Type_to_Onshore</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Damien Dau</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Acquire Inbound Web</value>
        </criteriaItems>
        <description>Workflow to update Lead Record Type to Onshore for Web leads with Damien Dau as the owner.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for Qualified Leads</fullName>
        <actions>
            <name>Update_Lead_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified for Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Employer,Time</value>
        </criteriaItems>
        <description>Workflow to update Lead Record Type to Onshore as Qualified for Contact.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for Spot Jobs Import</fullName>
        <actions>
            <name>Update_Lead_Record_Type_for_Spot_Jobs_Im</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Spot Jobs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Updates Lead Record Type to Data Analyst when Spot Jobs import data is inserted.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source if Advisor Creates</fullName>
        <actions>
            <name>Update_Lead_Source_Outbound_Referral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Source_for_Career_Advisor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Career Advisor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Lead Source to &quot;Outbound Referral&quot; if created by Career Advisor.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Acquire Bus</fullName>
        <actions>
            <name>Update_Account_Stage_Field6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Bus</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Acquire Bus&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Acquire Concept Shop</fullName>
        <actions>
            <name>Update_Account_Stage_Field4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Concept Shop</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Acquire Concept Shop&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Acquire Inbound Chat</fullName>
        <actions>
            <name>Update_Lead_Source_field1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Inbound Chat</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Acquire Inbound Chat&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Acquire Inbound Phone</fullName>
        <actions>
            <name>Update_Lead_Source_field2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Inbound Phone</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Acquire Inbound Phone&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Acquire Inbound Web</fullName>
        <actions>
            <name>Update_Lead_Source_field3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Inbound Web</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Acquire Inbound Web&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Acquire Kiosks</fullName>
        <actions>
            <name>Update_Account_Stage_Field7</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Acquire Kiosks</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Acquire Kiosks&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Australia Post</fullName>
        <actions>
            <name>Update_Account_Stage_Field8</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Australia Post</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Australia Post&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Carlton FC</fullName>
        <actions>
            <name>Update_Lead_Source_field10</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Carlton FC</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Carlton FC&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Gloria Jeans Coffees</fullName>
        <actions>
            <name>Update_Lead_Source_field9</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Gloria Jeans Coffees</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Gloria Jeans Coffees&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Melbourne Stars</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Melbourne Stars</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with Outbound Referral</fullName>
        <actions>
            <name>Update_Lead_Source_field4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>Outbound Referral</value>
        </criteriaItems>
        <description>Update Lead Source with &quot;Outbound Referral&quot; from the Lead Source for Career Advisor field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Source with ROKT</fullName>
        <actions>
            <name>Update_Lead_Source_field11</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Sources_for_Career_Advisor__c</field>
            <operation>equals</operation>
            <value>ROKT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Profile Fields when Data Analyst complete</fullName>
        <actions>
            <name>Update_Profile_DataAnalyst</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Profile_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Profile_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>4 AND (1 OR (2 AND 3))</booleanFilter>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified for Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status_Reason__c</field>
            <operation>equals</operation>
            <value>Not Eligible - Non Citizen,Under Age,Over Age,Over Qualified,Currently Studying,Trade-based,Incomplete Information,Work/Holiday Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Analyst,Auditor,Team Leader</value>
        </criteriaItems>
        <description>Update Profile Data Analyst, Profile Date &amp; Profile Status when Lead is profiled by Offshore data analyst. 

Note: This will only fire for users with profiles containing Acquire Data Analyst, Report Analyst, Auditor &amp; Team Leader.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>CallClosed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Closed Lead.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Call</subject>
    </tasks>
    <tasks>
        <fullName>No_Answer</fullName>
        <assignedToType>owner</assignedToType>
        <description>Called Lead and received No Answer.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>No Answer</subject>
    </tasks>
    <tasks>
        <fullName>Voicemail</fullName>
        <assignedToType>owner</assignedToType>
        <description>Called Lead and received Voicemail.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Voicemail</subject>
    </tasks>
</Workflow>
