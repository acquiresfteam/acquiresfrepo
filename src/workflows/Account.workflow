<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Sales_Admin_Events_of_deferred_student_resuming</fullName>
        <description>Alert Sales Admin - Events of deferred student resuming</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Admin_Events</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>salesforce@acquirelearning.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Deferred_Student_resume_alert</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_a_Sales_Admin</fullName>
        <description>Email alert to a Sales Admin</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_Sales_Admin</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_a_career_champion</fullName>
        <description>Email alert to a career champion</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_career_champion</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_notification_to_the_assigned_retention_specialist</fullName>
        <description>Send an email notification to the assigned retention specialist</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Owner_change_notification_to_Sales_Admin</template>
    </alerts>
    <fieldUpdates>
        <fullName>Acc_to_Employer</fullName>
        <description>Updates account to employer record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Employer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Acc to Employer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_to_Other</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PersonAccount</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Acc to Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AccountOwnerUpdate</fullName>
        <field>OwnerId</field>
        <lookupValue>tegan.swain@acquirelearning.com.au</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>AccountOwnerUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Champion_Date_Assignment_update</fullName>
        <description>Champion Date Assignment is update with current date</description>
        <field>Champion_Date_Assignment__c</field>
        <formula>TODAY()</formula>
        <name>Champion Date Assignment update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Course_Name_Update</fullName>
        <description>Updates the Course Name field on Account with the Related Course.</description>
        <field>Course_Name__c</field>
        <formula>Course_LookUp__r.Name</formula>
        <name>Course Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do_Not_Disturb_Date</fullName>
        <field>Do_Not_Disturb_Date__c</field>
        <formula>Today()</formula>
        <name>Do Not Disturb Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Education_Provider_Name_update</fullName>
        <description>Update Education Provider Name field with the Education Provider Account&apos;s name</description>
        <field>Education_Provider_Name__c</field>
        <formula>Education_Provider_Account__r.Name</formula>
        <name>Education Provider Name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Edvisor_Date_Assignment_update</fullName>
        <description>Edvisor Date Assignment update</description>
        <field>Edvisor_Date_Assignment__c</field>
        <formula>Today()</formula>
        <name>Edvisor Date Assignment update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_sent_check</fullName>
        <field>Has_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Email sent check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>First_CIP_Date_acc_update</fullName>
        <description>update &apos;First CIP Date&apos; field</description>
        <field>First_CIP_Date__c</field>
        <formula>TODAY()</formula>
        <name>First CIP Date acc update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>First_Champion_Date_Assignment_update_ac</fullName>
        <field>First_Champion_Date_Assignment_Acc__c</field>
        <formula>TODAY()</formula>
        <name>First Champion Date Assignment update ac</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hot_Star_Date</fullName>
        <field>Hot_Star_Date__c</field>
        <formula>Today()</formula>
        <name>Hot Star Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Career_Champion_ID</fullName>
        <field>Previous_record_ownerId__c</field>
        <name>Remove Career Champion ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Retention_Spec_Date_Assgn_Update</fullName>
        <description>Retention Specialist Date Assignment field is updated</description>
        <field>Retention_Specialist_Date_Assignment__c</field>
        <formula>TODAY()</formula>
        <name>Retention Spec Date Assgn Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Retention_Specialist_Name_removed</fullName>
        <field>Retension_Specialist_Name__c</field>
        <name>Retention Specialist Name removed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SMS_Field_Update_Checkbox</fullName>
        <description>Check &apos;SMS Sent&apos; field if Welcome SMS has been sent</description>
        <field>SMS_Sent__c</field>
        <literalValue>1</literalValue>
        <name>SMS Field Update Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_Name_removed</fullName>
        <field>Sales_Admin_Name__c</field>
        <name>Sales Admin Name removed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_s_name_captured</fullName>
        <field>Sales_Admin_Name__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Sales Admin&apos;s name captured</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetNoAnswerTrue</fullName>
        <field>No_Answer_Flag__c</field>
        <literalValue>1</literalValue>
        <name>SetNoAnswerTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetNoAnswerWFRule_cFalse</fullName>
        <field>NoAnswerWFRule__c</field>
        <literalValue>0</literalValue>
        <name>SetNoAnswerWFRule__cFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_ID_Number</fullName>
        <description>Populate ID Number field with unique ID formed by [record type abbr]+[autonumber]</description>
        <field>ID_Number__c</field>
        <formula>CASE($RecordType.Name, 
&quot;Person Account&quot;, &quot;STU&quot;&amp;ID_num__c, 
&quot;Employer&quot;, &quot;EMP&quot;&amp;ID_num__c, 
&quot;Education Provider Account&quot;, &quot;PRO&quot;&amp;ID_num__c, 
&quot;Business Account&quot;, &quot;BUS&quot;&amp;ID_num__c, 
&quot;Career Champion Account&quot;, &quot;CHA&quot;&amp;ID_num__c, 
NULL)</formula>
        <name>Set Account ID Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_1_Field_Update</fullName>
        <field>Current_Stage__c</field>
        <literalValue>Stage 1</literalValue>
        <name>Stage 1 Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_2_Field_Update</fullName>
        <field>Current_Stage__c</field>
        <literalValue>Stage 2</literalValue>
        <name>Stage 2 Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_3_Field_Update</fullName>
        <field>Current_Stage__c</field>
        <literalValue>Stage 3</literalValue>
        <name>Stage 3 Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_4_Field_Update</fullName>
        <field>Current_Stage__c</field>
        <literalValue>Complete</literalValue>
        <name>Stage 4 Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePrevAccOwnerId</fullName>
        <field>Previous_record_ownerId__c</field>
        <formula>Owner.Id</formula>
        <name>UpdatePrevAccOwnerId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date</fullName>
        <field>Date_of_Cancellation_Request__c</field>
        <formula>Today()</formula>
        <name>Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_record_Owner_Name</fullName>
        <field>Previous_record_ownerName__c</field>
        <formula>Owner.FirstName &amp; &apos; &apos; &amp;  Owner.LastName</formula>
        <name>Update Previous record Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Retention_ID</fullName>
        <field>Retension_Specialist_ID__c</field>
        <formula>OwnerId</formula>
        <name>Update Retention ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Retention_Name</fullName>
        <field>Retension_Specialist_Name__c</field>
        <formula>Owner.FirstName &amp; &apos; &apos; &amp;  Owner.LastName</formula>
        <name>Update Retention Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account created</fullName>
        <actions>
            <name>Set_Account_ID_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ID_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Call - Job follow up 90 days after RS complete</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Opportunity_Stage__c</field>
            <operation>equals</operation>
            <value>Rising Star Program Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Job_Placed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Course_Rising_Star_program_participant__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Rising Star is completed and Job Place is False, time dependent workflow after 90 days assign task to career champion.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Call_Job_Placement_follow_up_RisingStar</name>
                <type>Task</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Call - Job follow up after 160 days in prog</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Opportunity_Stage__c</field>
            <operation>equals</operation>
            <value>Course in Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Job_Placed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Course_Rising_Star_program_participant__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If person account course stage is equal to course in Progress, Job placed is False and days in program is 160. Assigned to &quot;Career Champion&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Call_Job_Placement_follow_up_RisingStar</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Rising_Star_Enrolment_Date__c</offsetFromField>
            <timeLength>160</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cancellation Requested Date Update</fullName>
        <actions>
            <name>Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Opportunity_Stage__c</field>
            <operation>equals</operation>
            <value>Cancellation Requested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capture Sales Admin name for cancellation confirmed and Course in progress</fullName>
        <actions>
            <name>Sales_Admin_s_name_captured</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( 
OR (   
ISPICKVAL( Opportunity_Stage__c ,&apos;Cancellation Confirmed&apos;) , ISPICKVAL( Opportunity_Stage__c ,&apos;Course in Progress&apos;)  
),  
CONTAINS( $Profile.Name , &quot;Sales Admin&quot;)  
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Champion Date %26 Name Assignment Rule</fullName>
        <actions>
            <name>Champion_Date_Assignment_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_record_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Champion Date Assignment is updated with current date when owner is changed to Career Champion.</description>
        <formula>AND (CONTAINS(Owner.UserRole.Name, &quot;Career Champion&quot;), ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Course Name Update</fullName>
        <actions>
            <name>Course_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the course name with the value in the Related Course field.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create No Answer Task on Account</fullName>
        <actions>
            <name>SetNoAnswerTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetNoAnswerWFRule_cFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>No_Answer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.NoAnswerWFRule__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow creates a No Answer task and then sets the field &quot;NoAnswerWFRule__c&quot; back to false.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Current Stage 1</fullName>
        <actions>
            <name>Stage_1_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>if( AND(RSP_Stage_1_Complete__c == false , RSP_Stage_2_Complete__c == false , RSP_Stage_3_Complete__c == false ) , true , false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Current Stage 2</fullName>
        <actions>
            <name>Stage_2_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>if( AND(RSP_Stage_1_Complete__c == true , RSP_Stage_2_Complete__c == false , RSP_Stage_3_Complete__c == false ) , true , false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Current Stage 3</fullName>
        <actions>
            <name>Stage_3_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>if( AND(RSP_Stage_1_Complete__c == true, RSP_Stage_2_Complete__c == true , RSP_Stage_3_Complete__c == false ) , true , false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Current Stage 4</fullName>
        <actions>
            <name>Stage_4_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>if( AND(RSP_Stage_1_Complete__c == true, RSP_Stage_2_Complete__c == true, RSP_Stage_3_Complete__c == true) , true , false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Deferred Student resumes</fullName>
        <actions>
            <name>Alert_Sales_Admin_Events_of_deferred_student_resuming</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Opportunity_Stage__c,&quot;Course in Progress&quot;),ISPICKVAL(PRIORVALUE(Opportunity_Stage__c),&quot;Course Deffered Confirmed&quot;),   OR(CONTAINS(Course_LookUp__c , &quot;Venture&quot;),Course_LookUp__c = &quot;Diploma of Business (Entrepreneurship&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Disturb Date</fullName>
        <actions>
            <name>Do_Not_Disturb_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Do_Not_Disturb__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Education Provider Name Rule for Partners</fullName>
        <actions>
            <name>Education_Provider_Name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Edvisor Name Assignment Rule</fullName>
        <actions>
            <name>Edvisor_Date_Assignment_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_record_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Champion / Edvisor name is populated with Edvisor name when owner is changed to Edvisor.</description>
        <formula>AND (CONTAINS(Owner.UserRole.Name, &quot;Eddi Edvisor&quot;), ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>First CIP Date Rule</fullName>
        <actions>
            <name>First_CIP_Date_acc_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update &apos;First CIP Date&apos; field to current date when account stage is changed to &apos;Course in Progress&apos; for the first time</description>
        <formula>AND(  ISCHANGED(Opportunity_Stage__c),  ISPICKVAL(Opportunity_Stage__c, &apos;Course in Progress&apos;),  ISPICKVAL(PRIORVALUE(Opportunity_Stage__c), &apos;&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>First Champion Date Assignment Rule</fullName>
        <actions>
            <name>Email_sent_check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>First_Champion_Date_Assignment_update_ac</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (CONTAINS(Owner.UserRole.Name, &quot;Career Champion&quot;),  ISCHANGED(OwnerId),  Has_Email_Sent__c = false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Hot Star Date</fullName>
        <actions>
            <name>Hot_Star_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Hot_Star__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead to Acc Emp Rec Type</fullName>
        <actions>
            <name>Acc_to_Employer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Acc_long__c</field>
            <operation>equals</operation>
            <value>Employer</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Acquire Business Builder</value>
        </criteriaItems>
        <description>For Business Builders: changes record type of account to employer if lead was an employer.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead to Acc Other Rec Type</fullName>
        <actions>
            <name>Acc_to_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Account.Record_Type_Acc_long__c</field>
            <operation>equals</operation>
            <value>Career Adviser</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_Acc_long__c</field>
            <operation>equals</operation>
            <value>Data Analyst</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Record_Type_Acc_long__c</field>
            <operation>equals</operation>
            <value>Sales Admin</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Acquire Business Builder</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OpportunityStageValueChange</fullName>
        <actions>
            <name>Email_alert_to_a_Sales_Admin</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AccountOwnerUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Opportunity_Stage__c</field>
            <operation>equals</operation>
            <value>Course Deffered Requested,Cancellation Requested</value>
        </criteriaItems>
        <description>If the value of opportunity stage changes to Course Deffered Requested and Cancellation Requested, Account Owner will be changed to Tegan Swain</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OpportunityStageValueChange1</fullName>
        <actions>
            <name>UpdatePrevAccOwnerId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_record_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>when Retention Specialist changes the Opportunity stage to Course in Progress, Account Owner will be changed to Career Champion</description>
        <formula>AND( OR(Owner.Profile.Name == &apos;Acquire Career Champion&apos;,Owner.Profile.Name == &apos;Eddi Edvisor&apos;,Owner.Profile.Name == &apos;Acquire Career Advisor&apos;), OR( ISPICKVAL( Opportunity_Stage__c , &apos;Course Deffered Requested&apos;), ISPICKVAL( Opportunity_Stage__c , &apos;Cancellation Requested&apos;)  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Owner change notification to career champion%2FEdvisor</fullName>
        <actions>
            <name>Email_alert_to_a_career_champion</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Remove_Career_Champion_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updated to include Edvisor where relevant</description>
        <formula>AND(  OR(Owner.Profile.Name == &quot;Acquire Career Champion&quot;,Owner.Profile.Name == &quot;Eddi Edvisor&quot;), ISPICKVAL(Opportunity_Stage__c,&apos;Course in Progress&apos;),  NOT(ISBLANK( Previous_record_ownerId__c ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove sales admin name and retention specialist name if cancellation requested</fullName>
        <actions>
            <name>Retention_Specialist_Name_removed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_Admin_Name_removed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( OR($Profile.Name = &apos;Acquire Career Champion&apos;,$Profile.Name = &apos;Eddi Edvisor&apos;), ISPICKVAL(Opportunity_Stage__c , &apos;Cancellation Requested&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Retention Specialist Date %26 Name Assignment Rule</fullName>
        <actions>
            <name>Retention_Spec_Date_Assgn_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Retention_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&apos;Retention Specialist Date Assignment&apos; field is updated with the current date when owner of record is changed to a Retention Specialist</description>
        <formula>AND (CONTAINS( Owner.Profile.Name , &quot;Retention Specialist&quot;),  ISCHANGED(OwnerId) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Welcome SMS from Career Champion</fullName>
        <actions>
            <name>SMS_Field_Update_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Welcome_SMS</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Welcome SMS to Non-Eddi Rising Star from Career Champion when first assigned to him/her</description>
        <formula>AND( 
SMS_Sent__c = FALSE, 
Owner.Profile.Name = &quot;Acquire Career Champion&quot;,
ISPICKVAL(Opportunity_Stage__c, &apos;Course in Progress&apos;),
NOT(AND(!CONTAINS(Education_Provider_Name__c, &quot;Non Eddi&quot;),CONTAINS(Education_Provider_Name__c, &quot;Eddi&quot;)))
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send notification email to Retention Speacialist</fullName>
        <actions>
            <name>Send_an_email_notification_to_the_assigned_retention_specialist</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to send a notification email to the retention specialist, when an Account with &quot;Cancellation Request&quot; or &quot;Course Deferred Requested&quot; is assigned.</description>
        <formula>AND(  Owner.Profile.Name == &quot;Acquire Retention Specialist&quot;,  NOT(ISBLANK( Previous_record_ownerId__c )),  OR(  ISPICKVAL(Opportunity_Stage__c,&apos;Course Deffered Requested&apos;),  ISPICKVAL(Opportunity_Stage__c,&apos;Cancellation Requested&apos;)  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Retention Specialist Name</fullName>
        <actions>
            <name>Update_Retention_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Retention_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( TEXT(Opportunity_Stage__c) == &apos;Course in Progress&apos;,  Owner.Profile.Name == &apos;Acquire Retention Specialist&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Star Status Hidden</fullName>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6</booleanFilter>
        <criteriaItems>
            <field>Account.STAR_Status__c</field>
            <operation>equals</operation>
            <value>/img/samples/stars_000.gif</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.STAR_Status__c</field>
            <operation>equals</operation>
            <value>/img/samples/stars_100.gif</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.STAR_Status__c</field>
            <operation>equals</operation>
            <value>/img/samples/stars_200.gif</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.STAR_Status__c</field>
            <operation>equals</operation>
            <value>/img/samples/stars_300.gif</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.STAR_Status__c</field>
            <operation>equals</operation>
            <value>/img/samples/stars_400.gif</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.STAR_Status__c</field>
            <operation>equals</operation>
            <value>/img/samples/stars_500.gif</value>
        </criteriaItems>
        <description>Used for reporting for career champions</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Call_Job_Placement_follow_up_RisingStar</fullName>
        <assignedTo>Career_Champion</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Call - Job Placement follow up</subject>
    </tasks>
    <tasks>
        <fullName>No_Answer</fullName>
        <assignedToType>owner</assignedToType>
        <description>Called Lead and received No Answer.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>No Answer</subject>
    </tasks>
    <tasks>
        <fullName>Welcome_SMS</fullName>
        <assignedTo>syed.ahmed@acquirelearning.com.au</assignedTo>
        <assignedToType>user</assignedToType>
        <description>SMS-Notification-Account-4fb5</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Welcome SMS</subject>
    </tasks>
</Workflow>
