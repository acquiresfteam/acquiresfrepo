<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Acquire_Points_to_Blank</fullName>
        <field>Points__c</field>
        <name>Update Acquire Points to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Roll-Back Date 3 months</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Acquire_Achievements__c.Roll_Back_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Acquire_Achievements__c.Type__c</field>
            <operation>equals</operation>
            <value>Late,Absent,Half-Day,Undertime,NCNS</value>
        </criteriaItems>
        <description>Object: Acquire Attendance Points Field: Roll-Back Date. Update Points field to zero once it reaches its related Roll-Back Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Acquire_Points_to_Blank</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Acquire_Achievements__c.Roll_Back_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
