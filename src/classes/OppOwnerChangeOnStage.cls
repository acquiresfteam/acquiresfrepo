global class OppOwnerChangeOnStage implements Database.Batchable<sObject>{

global Database.QueryLocator start(Database.BatchableContext BC){
System.debug('**************'+'OpptSetting');
Opportunity__c OpptSetting =[select Opportunity_Age_Count__c from Opportunity__c limit 1]; 
return Database.getQueryLocator([Select Opportunity_Age__c,Owner.profile.name,StageName,RecordType.Name,OwnerId from Opportunity]);

}
global void execute(Database.BatchableContext info, List<sObject> scope){
List<Id> oppOwnerIds = new List<Id>();
List<Opportunity> OppList = new List<Opportunity>();
Opportunity__c OpptSetting =[select Opportunity_Age_Count__c from Opportunity__c limit 1];
User Usr =[select id, FirstName,LastName from User where FirstName='Sarah' and LastName='Jenkins'];
System.debug('&&&&&&&&&'+'Oppt');
for(sobject O: scope){
Opportunity Oppt =(Opportunity)O;
//for(Opportunity Op: scope.values()){
//oppOwnerIds.add(Op.ownerid);
//Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id in :oppOwnerIds]);
 //for(Opportunity newopp: scope.Values()){
           //String newOwnerProfile = (mpuser.get(scope.get(newopp.id).ownerid).profile.name).trim(); 
           
     

if(Oppt.Opportunity_Age__c > OpptSetting.Opportunity_Age_Count__c && Oppt.Owner.profile.name != 'Acquire Retention Specialist' 
 && (Oppt.StageName == 'Contacted - Info Sent' || Oppt.StageName == 'Enrolled - Forms Completed')&&Oppt.RecordType.Name != 'Employer') {
              Oppt.OwnerId =Usr.id;
              OppList.add(Oppt);
    }

update OppList;
//}
//}
}
}
global void finish(Database.BatchableContext BC){
   
   }
   }