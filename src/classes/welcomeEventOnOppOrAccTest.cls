@istest
public class welcomeEventOnOppOrAccTest 
{
    public static testMethod void welcomeEventOnOppTest() 
    {
        
        RecordType opprt = [select id,Name from RecordType where SobjectType='Opportunity' and Name='Default' Limit 1];

        Opportunity Oppt = new Opportunity();
        Oppt.RecordTypeId = opprt.id;
        Oppt.StageName='Contacted - Info Sent';
        Oppt.CloseDate = system.today();
        Oppt.Name ='sample';
        insert Oppt;

        RecordType evrt = [select id,Name from RecordType where SobjectType='Event' and Name='Welcome Call Event' Limit 1];

        Event ev = new Event();
        ev.RecordTypeId = evrt.id;
        ev.Subject = 'Welcome Call Booking';
        ev.WhatId=Oppt.Id;
        ev.DurationInMinutes=5;
        ev.ActivityDateTime=datetime.now();
        insert ev;
        
        Opportunity createdOpp = [Select Id,Name,Welcome_Call_scheduled__c From Opportunity Where Id = :Oppt.Id];
        
        System.debug('Opportunity ' + createdOpp.Id + ' ' + createdOpp.Name + ' - Welcome call scheduled: ' + createdOpp.Welcome_Call_scheduled__c);
    }
    
    public static testMethod void welcomeEventOnAccTest() 
    {
        
        RecordType personacc = [select id,Name from RecordType where SobjectType='Account' and Name='Person Account' Limit 1];

		Account Acc = new Account();
		Acc.RecordTypeId = personacc.id;
		Acc.LastName='Test';
		insert Acc;

        RecordType evrt = [select id,Name from RecordType where SobjectType='Event' and Name='Welcome Call Event' Limit 1];

        Event ev = new Event();
        ev.RecordTypeId = evrt.id;
        ev.Subject = 'Welcome Call Booking';
        ev.WhatId=Acc.Id;
        ev.DurationInMinutes=5;
        ev.ActivityDateTime=datetime.now();
        insert ev;
        
        Account createdAcc = [Select Id,LastName,Welcome_Call_booked_on_account__c From Account Where Id = :Acc.Id];
        
        System.debug('Account ' + createdAcc.Id + ' ' + createdAcc.LastName + ' - Welcome call scheduled: ' + createdAcc.Welcome_Call_booked_on_account__c);
    }
}