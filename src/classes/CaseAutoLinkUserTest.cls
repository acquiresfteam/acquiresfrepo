@istest(SeeAllData=true)
public class CaseAutoLinkUserTest 
{
    public static testMethod void testUserGetsLinked() 
    {
        
        Test.startTest();

        User testUser = new User(Username= 'testUser1@company.com.CaseAutoLinkUserTest',
        Email = 'testUser1@company.com.CaseAutoLinkUserTest',
        Lastname = 'user',
        Firstname = 'test',
        Alias = 'test',
        CommunityNickname = '12346',
        ProfileId = [Select Id from Profile where Name = 'System Administrator'].Id,

        TimeZoneSidKey = 'GMT',
        LocaleSidKey = 'en_US',
        EmailEncodingKey = 'ISO-8859-1',
        LanguageLocaleKey = 'en_US',
        UserPermissionsMobileUser = false);

        insert testUser;

        Case c = new Case(SuppliedEmail='testUser1@company.com.CaseAutoLinkUserTest',
                            Subject='Feedback - Something',Origin='Email', recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name = 'Internal SF Support' limit 1].Id);
        insert c;
        
        Case createdCase = [Select Id,User_Effected__c,Salesforce_Profile__c From Case Where Id = :c.Id];
        
        System.debug(createdCase.Id + ' ' + createdCase.User_Effected__c + ' ' + createdCase.Salesforce_Profile__c);
            System.assert(createdCase.User_Effected__c!=null,'User should not be null');
            System.assert(createdCase.Salesforce_Profile__c!=null,'Profile should not be null');

            Test.stopTest();
    }
}