/*
Batchable class to update the following fields on the Lead object 
past 14 days after setting the Status ='Unable to Contact'
a. Clear Notes__c field
b. Status = 'Qualified for Contact'
c. Owner  =  Queue Id of Queue 'AU Outbound Unable to Contact'
d. Clear Activity History(Tasks) list
e. Clear Date_Unable_to_Call__c field
@author: SMendu 
@CreatedDate: 16/04/2015
*/
global class BatchActivityOnLeadStatusNAge implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
    	
    	//Configuration for - LeadAge & LeadStatus
        LeadStatusNAge__c leadStatusNAgeCfg = LeadStatusNAge__c.getInstance('Configuration');
        Integer leadAge = (Integer) leadStatusNAgeCfg.LeadAge__c;
        String leadStatus = leadStatusNAgeCfg.LeadStatus__c;
        
        System.debug('LeadStatusNAge__c Custom settings::LeadAge== '+leadAge+'LeadStatus=='+leadStatus);
        
        String queryStr = 'Select Notes__c, Status, Owner.Id,Date_Unable_to_Call__c,(Select Id, Subject from Tasks) from LEAD where Status =\''+ leadStatus
         + '\' and Date_Unable_to_Call__c <= LAST_N_DAYS:' + leadAge + ' and Owner.Type!=\'Queue\'';
  		
  		Database.QueryLocator queryLocator = Database.getQueryLocator(queryStr);
  	    
  	    return queryLocator;
    }
 
    global void execute(Database.BatchableContext info, List<sObject> scope){
    	 List<Lead> leadsList = new List<Lead>();     
    	 List<Task> taskList = new List<Task>();
    	 QueueSobject unableToContactQ = [SELECT Id,queue.Name,SobjectType,QueueId  FROM QueueSobject WHERE SobjectType = 'Lead'and queue.Name = 'AU Outbound Unable to Contact'];
    	 
         for(SObject scopeLead: scope){
            Lead lead =(Lead)scopeLead;
            System.debug('Lead In Scope=='+lead);  
            lead.Notes__c = null;
            lead.Status ='Qualified for Contact';
            lead.OwnerId = unableToContactQ.QueueId;
            lead.Date_Unable_to_Call__c = null;
            taskList.addAll(lead.Tasks);
            leadsList.add(lead);
          }
 	     delete taskList;
 	     update leadsList;
    }
    
      global void finish(Database.BatchableContext BC){
   	  } 
     
}