/*
Schedulable class to check for Leads past 14 days with Status ='Unable to Contact'
@author: SMendu 
@CreatedDate: 16/04/2015
*/
global class ScheduleActivityOnLeadStatusNAge implements Schedulable {
	
 public void execute(SchedulableContext SC){
     BatchActivityOnLeadStatusNAge batchActivity = new BatchActivityOnLeadStatusNAge ();
     Database.executeBatch(batchActivity);
     }
     
}