@isTest
public class AccountTrgUtil_Test{

    public static testmethod void RetentionTest(){
    
        //Profile profile = [SELECT Id FROM Profile WHERE Name='Acquire Career Advisor']; 
        Profile profile = QueryUtility.getProfileByName('Acquire Career Advisor');
        
        User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = profile.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        insert user;
        
        RecordType recType = [select id from RecordType where DeveloperName = 'PersonAccount' LIMIT 1];
        
        
        Account Acc = new Account();
        Acc.recordtypeid = recType.id;
        Acc.LastName='Test';
        Acc.Opportunity_Stage__c = 'Course in Progress';
        //Acc.Previous_record_ownerId__c = '00590000002DA6M';
        //Acc.OwnerId = '00590000001T5iA';
        //Acc.Retension_Specialist_ID__c = '00590000002DEUfAAO';
        Acc.Retension_Specialist_Name__c = 'Retention';
        //Acc.OwnerId = Acc.Previous_record_ownerId__c;  
        insert Acc ;
        
        AccountTrgUtil.flag = false;
        
        Opportunity Opp = new Opportunity();
        //Opp.Account = Acc.Name;
        Opp.OwnerId = user.id;
        Opp.Name = 'Oppor';
        Opp.accountId = Acc.id;
        opp.StageName = 'Enrolled - Confirmed';
        Opp.CloseDate = system.today();
        Opp.X1st_census_date__c = system.today();
        Opp.X2nd_census_date__c = system.today();
        Opp.Cancelation_Reasons__c = 'Unaware of enrolment';
        Opp.Source_of_Cancelation__c = 'Career Champion';
        Opp.Enrolled_Awaiting_ID__c = false;
        Opp.Enrolled_Awaiting_VET_Fee__c = false;
        Opp.Enrolled_Incorrect_Course__c = false;
        insert Opp ;
        
        Acc.Recent_Closed_Opportunity_Id__c = opp.id;
        Acc.Opportunity_Stage__c = 'Cancellation Requested';
        Acc.Opportunity_Cancellation_Reason__c = 'Unaware of enrolment';
        Acc.Source_of_Cancelation__c = 'Career Champion';
        update Acc;
        
        Test.startTest();
        
        System.runAs(user) {
            Opp.OwnerId = user.id;
            Opp.StageName = 'Cancellation Requested';
            update Opp;    
        }
        
         test.stopTest();
    }
    
    public static testmethod void CampaingMember(){
    
        // Profile profilecamp = [SELECT Id, Name FROM Profile WHERE Name=: 'Acquire Career Champion'];
        Profile profilecamp = QueryUtility.getProfileByName('Acquire Career Champion');
    
        User userdetails = new User(Alias = 'standt1', Email='test.user1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US',ProfileId = profilecamp.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        insert userdetails;
        
        Account Acc = new Account();
  
        Acc.LastName = 'test';
        insert Acc;
           
     /*   Lead Camplead = new Lead();
        Camplead.Lastname= 'Test';
        Camplead.Firstname= 'Lead';
        Camplead.Status = 'New';
        insert Camplead;
        
        CampaignMember CampMem = new CampaignMember();
        CampMem.Status = 'Sent';
        CampMem.CampaignId =Camplead.Id ;
        insert CampMem;*/
        
        Campaign Camp = new Campaign ();
        //Camp.id = OwnerId;
        //Camp.Id= Camplead.Id;
        Camp.name = 'Student Welcome Email';
        insert Camp;
        
        Test.startTest();
        
        Acc.OwnerId = userdetails.Id;
	    
        Update Acc;
        test.stopTest();
   
    }
   
     public static testmethod void VentureCampaingMember(){
    
        
        Account Acct= new Account();
        Acct.LastName = 'test';
        Acct.Opportunity_Stage__c = 'Course in Progress';
        Acct.Is_Venture_Campaign_Member_Created__c= false;
       //Acct.Education_Provider_Name__c='Careers Australia';
        //Acct.Course_Rising_Star_program_participant__c = true;
        
        
        insert Acct;
        
        
     /*   Lead Camplead = new Lead();
        Camplead.Lastname= 'Test';
        Camplead.Firstname= 'Lead';
        Camplead.Status = 'New';
        insert Camplead;
        
        CampaignMember CampMem = new CampaignMember();
        CampMem.Status = 'Sent';
        CampMem.CampaignId =Camplead.Id ;
        insert CampMem;*/
        
           
        Campaign Camp1 = new Campaign ();
        //Camp.id = OwnerId;
        //Camp.Id= Camplead.Id;
        Camp1.name = 'Venture Email';
        
        Test.startTest();
        
        // Camp1.Education_Provider_Name__c = Acct.Education_Provider_Name__c;
        insert Camp1;
        Camp1.Status = 'Sent';
        update Camp1;
        test.stopTest();
    } 
  
  
    public static testmethod void NextActivityTest(){
        Account acc = new Account();
        acc.X01_Welcome_Call_And_Email__c= '0';
        acc.Next_Activity__c='02 Induction Call';
        acc.X02_Induction_Call__c='0';
        acc.X03_Goal_Setting__c='0';
        acc.X04_Personality_Profiling__c='0';
        acc.X05_Social_Media_Strategy__c='0';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='0';
        acc.X07_Interview_Techniques_And_Tips__c='0';
        acc.X08_Dress_for_Success__c='0';
        acc.X09_Job_Search_Techniques__c='0';
        acc.X10_Hidden_Job_Approach__c='0';
        acc.X11_Keeping_a_Job__c='0';
        acc.X12_Internships_or_Acquire_Experiences__c='0';
        acc.X13_Role_Play_for_Interview__c='0';
        acc.Industry_Preferences__c='Construction';
        acc.Sub_Category_1__c='Estimating';
        acc.RecordTypeId = '01290000000XDGF';
        acc.Name='test';
        ////acc.Id='001N000000E5P4I';
        //insert acc;
        
        acc.X01_Welcome_Call_And_Email__c= '0';
        //update acc;
        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='0';
        //update acc;

        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='0';
        //update acc;
        
        
        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='1';
        acc.X04_Personality_Profiling__c='1';
        acc.X05_Social_Media_Strategy__c='1';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='1';
        acc.X07_Interview_Techniques_And_Tips__c='1';
        acc.X08_Dress_for_Success__c='1';
        acc.X09_Job_Search_Techniques__c='0';
        //update acc;
        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='1';
        acc.X04_Personality_Profiling__c='1';
        acc.X05_Social_Media_Strategy__c='1';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='1';
        acc.X07_Interview_Techniques_And_Tips__c='1';
        acc.X08_Dress_for_Success__c='1';
        acc.X09_Job_Search_Techniques__c='1';
        acc.X10_Hidden_Job_Approach__c='0';
        //update acc;
        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='1';
        acc.X04_Personality_Profiling__c='1';
        acc.X05_Social_Media_Strategy__c='1';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='1';
        acc.X07_Interview_Techniques_And_Tips__c='1';
        acc.X08_Dress_for_Success__c='1';
        acc.X09_Job_Search_Techniques__c='1';
        acc.X10_Hidden_Job_Approach__c='1';
        acc.X11_Keeping_a_Job__c='0';
        //update acc;
        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='1';
        acc.X04_Personality_Profiling__c='1';
        acc.X05_Social_Media_Strategy__c='1';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='1';
        acc.X07_Interview_Techniques_And_Tips__c='1';
        acc.X08_Dress_for_Success__c='1';
        acc.X09_Job_Search_Techniques__c='1';
        acc.X10_Hidden_Job_Approach__c='1';
        acc.X11_Keeping_a_Job__c='1';
        acc.X12_Internships_or_Acquire_Experiences__c='0';
        //update acc;
        
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='1';
        acc.X04_Personality_Profiling__c='1';
        acc.X05_Social_Media_Strategy__c='1';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='1';
        acc.X07_Interview_Techniques_And_Tips__c='1';
        acc.X08_Dress_for_Success__c='1';
        acc.X09_Job_Search_Techniques__c='1';
        acc.X10_Hidden_Job_Approach__c='1';
        acc.X11_Keeping_a_Job__c='1';
        acc.X12_Internships_or_Acquire_Experiences__c='1';
        acc.X13_Role_Play_for_Interview__c='0';
        //update acc;
              
        acc.X01_Welcome_Call_And_Email__c= '1';
        acc.X02_Induction_Call__c='1';
        acc.X03_Goal_Setting__c='1';
        acc.X04_Personality_Profiling__c='1';
        acc.X05_Social_Media_Strategy__c='1';
        acc.X06_LinkedIn_Resume_Cover_Letter__c='1';
        acc.X07_Interview_Techniques_And_Tips__c='1';
        acc.X08_Dress_for_Success__c='1';
        acc.X09_Job_Search_Techniques__c='1';
        acc.X10_Hidden_Job_Approach__c='1';
        acc.X11_Keeping_a_Job__c='1';
        acc.X12_Internships_or_Acquire_Experiences__c='1';
        acc.X13_Role_Play_for_Interview__c='1';
        acc.Next_Activity__c = 'No Activity';
        //update acc;
        insert acc;
        
        Test.startTest();
                
        Careers__c crs = new Careers__c();
        crs.Student_Name__c =acc.Id;
        insert crs;
        
        test.stopTest();
    }
    
    public static testMethod void updateOppOwnerTest(){
       // Profile hunterProfile = [SELECT Id, Name FROM Profile WHERE Name=: 'Acquire Career Hunter']; 
          Profile hunterProfile = QueryUtility.getProfileByName('Acquire Career Hunter');
        
       // Profile shortlisterProfile = [SELECT Id, Name FROM Profile WHERE Name=: 'Acquire Career Qualifiers']; 
          Profile shortlisterProfile = QueryUtility.getProfileByName('Acquire Career Qualifiers');

        User user1 = new User(Alias = 'standt1', Email='test.user1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = hunterProfile.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning1@testorg.com');
        insert user1;

        User user2 = new User(Alias = 'standt2', Email='test.user2@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = shortlisterProfile.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning2@testorg.com');
        insert user2;


        RecordType accRecType = [Select id From RecordType where sObjectType = 'Account' AND DeveloperName = 'PersonAccount' LIMIT 1];
        
        RecordType oppRecType = [Select id From RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Employer' LIMIT 1];
        
        
        Account acc = new Account();
        acc.recordtypeid = accRecType.id;
        acc.LastName='Test';
        acc.Opportunity_Stage__c = 'Course in Progress';
        insert acc ;

        Opportunity opp = new Opportunity();
        opp.recordtypeid = oppRecType.id;
        opp.Name = 'Oppor';
        opp.accountId = Acc.id;
        opp.StageName = 'Job Placed';
        opp.CloseDate = system.today();
        opp.X1st_census_date__c = system.today();
        opp.X2nd_census_date__c = system.today();
        opp.Career_Hunter_Owner_ID__c = user1.id;
        insert opp ;
        
        acc.OwnerId = user2.id;
        update acc;
        
        opp.OwnerId = user2.id;
        update opp;

        Test.startTest();

        System.runAs(user2){
            opp.StageName = 'Job Shortlisted';
            update opp;
        }   

        Test.stopTest();
    } 

}