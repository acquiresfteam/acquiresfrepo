@isTest
public class BatchActivityonLeadAge_Test{

public static testmethod void UnitTest(){

RecordType rt = [select id,Name from RecordType where SobjectType='Lead' and Name='Employer' Limit 1];
    
Lead Ld = new Lead();
Ld.RecordTypeId = rt.id;
Ld.Status='New';
Ld.LastName='test';
Ld.Phone='9999999999';
Ld.LeadSource ='Acquire Inbound Phone';
insert Ld ;
 
List<Task> Tasks = new List<Task>();
Tasks.add(new Task(
ActivityDate = Date.today().addDays(1),
WhoId = Ld.id,
Status = 'Not Started',
type='Other',
Priority='Normal',
Subject='No Answer'
));
insert Tasks;
    
Lead__c leadage= new Lead__c();
leadage.Lead_Age__c = -1;
insert leadage;      

    /*
Group grp= new Group();
grp.Name='AU Inbound Unable to Contact';
grp.Type='Queue';
insert grp;
*/

Test.startTest();
Id batchInstanceId = Database.executeBatch(new BatchActivityonLeadAge());
Test.stopTest();

  }      

}