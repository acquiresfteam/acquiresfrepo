/* Testing this commit to Master removing branches**/
@istest
public class AccountTeam_test 
{
    public static testMethod void accountTeamMemberCreateTest() 
    {
       // Profile profile = [SELECT Id FROM Profile WHERE Name='Acquire Career Champion']; 
        Profile profile = QueryUtility.getProfileByName('Acquire Career Champion');
        
        User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        insert user;
        
        RecordType personacc = [select id,Name from RecordType where SobjectType='Account' and Name='Person Account' Limit 1];
        
        Account Acc = new Account();
        Acc.RecordTypeId = personacc.id;
        Acc.LastName='Test';
        insert Acc;
        
        Account createdAcc = [Select Id,LastName,OwnerId From Account Where Id = :Acc.Id];
        createdAcc.OwnerId = user.Id;
        update createdAcc;
        
        Account createdAcc2 = [Select Id,LastName,OwnerId From Account Where Id = :CreatedAcc.Id];
        AccountTeamMember createdATM = [Select Id,AccountId, UserId, AccountAccessLevel, TeamMemberRole From AccountTeamMember Where AccountId = :createdAcc2.Id Limit 1];
                
        System.debug('Account Owner' + createdAcc2.OwnerId + ' is an Account Team Member with role ' + createdATM.TeamMemberRole + ' and access level ' + createdATM.AccountAccessLevel );
        system.assertEquals (createdAcc2.OwnerId,createdATM.UserId);
        system.assertEquals (createdATM.TeamMemberRole,'Career Champion');
    }
}