@isTest
Public class CountOnNoAnswer_Test{

public static testmethod void UnitTest(){
Test.startTest();

//Create Lead
Lead Ld = new Lead();
Ld.Status='New';
Ld.LastName='test';
Ld.Phone='9999999999';
Ld.LeadSource ='test Inbound tt'; 
Ld.Company='Test Company';
insert Ld ;

//create Account
Account acc = new Account();
acc.firstName = 'Test';
acc.LastName = 'Account';
insert acc;
    
Task tsk = new Task();
tsk.OwnerId= UserInfo.getUserId();
tsk.Subject = 'No Answer';
tsk.Status = 'Completed';
tsk.Priority = 'High';
tsk.whoid = Ld.id;
insert tsk;

Task tsk1 = new Task();
tsk1.OwnerId= UserInfo.getUserId();
tsk1.Subject = 'No Answer';
tsk1.Status = 'Completed';
tsk1.Priority = 'High';
tsk1.whatid = acc.id;
insert tsk1;

test.stopTest();

}
}