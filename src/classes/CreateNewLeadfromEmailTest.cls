/************************
    * Class:        CreateNewLeadfromEmailTest
    * Author:       Michael Cooksley (Cloud Sherpas)
    * Description:  Test class for CreateNewLeadfromEmail Class.
    ************************/
    
@isTest
private class CreateNewLeadfromEmailTest {

static testMethod void firstTest() {
	  	
	  	// Setup data in Meta lead source table.	
    	List<Meta_Lead_Source__c> insertMetaLeadSource = new List<Meta_Lead_Source__c>();
        Meta_Lead_Source__c mLS1 = new Meta_Lead_Source__c(Category__c = '', Domain__c = 'recruiteasy.com.au', Lead_Source__c = 'Recruit Easy', Queue_Name__c = '', Type__c = 'Lead Source');
        insertMetaLeadSource.add(mLS1);
        Meta_Lead_Source__c mLS2 = new Meta_Lead_Source__c(Category__c = '', Domain__c = 'recruiteasy.com.au', Lead_Source__c = 'Recruit Easy', Queue_Name__c = '', Type__c = 'Lead Source');
        insertMetaLeadSource.add(mLS2);      
        insert insertMetaLeadSource;
	  		  	
	  	// Setup the attachments.
	  	CreateNewLeadfromEmail emailService = new CreateNewLeadfromEmail();
        Messaging.Inboundemail.Binaryattachment binaryAttachment = new Messaging.Inboundemail.Binaryattachment();
        binaryAttachment.body = Blob.valueOf('UNIT TEST BINARY ATTACHMENT');
        binaryAttachment.fileName = 'A_UnitTest.gif';
 
        Messaging.Inboundemail.Textattachment textAttachment = new Messaging.Inboundemail.Textattachment();
        textAttachment.body = 'UNIT TEST TEXT ATTACHMENT';
        textAttachment.fileName = 'A_UnitTest.txt';
    	
    	// Send a valid message.
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = 'itapplicants@recruiteasy.com.au';
        email.toAddresses = new List<String>{'itapplicants@recruiteasy.com.au'};
        email.subject = 'Subject';
        email.plainTextBody = 'Application Date: 10/10/2013\n'+
        'Applicant Name: Ricardo Tubbs\n' +
        'Email: rtubbs@hotmail.com\n' +
        'Phone: 0404894920\n';
        email.binaryAttachments = new List<Messaging.Inboundemail.Binaryattachment>{binaryAttachment};
        email.textAttachments = new List<Messaging.Inboundemail.Textattachment>{textAttachment};
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        emailService.handleInboundEmail(email, envelope);
        
        /* Validation Rule *Landline_Phone_Validation_Rule* made block useless
        // Send a failed message to hit the exception code.
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        email2.fromAddress = 'itapplicants@recruiteasy.com.au';
        email2.toAddresses = new List<String>{'itapplicants@recruiteasy.com.au'};
        email2.subject = 'Subject';
        email2.plainTextBody = 'Application Date: 10/10/2013\n'+
        'Applican: Ricardo Tubbs\n' +
        'Ema: rtubbs@hotmail.com\n' +
        'Phone: 0404894920\n';
        Messaging.InboundEnvelope envelope2 = new Messaging.InboundEnvelope();
        emailService.handleInboundEmail(email2, envelope2);
        */
    }
}