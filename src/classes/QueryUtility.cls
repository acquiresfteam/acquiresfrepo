/*
Description:	Utility class to cache the result object of a SOQL query against a key in a map
@author: 		SMendu 
@CreatedDate: 	9/04/2015
*/
public class QueryUtility {
      
      	private static Map<String, Profile> profileResultByNameMap = new Map<String, Profile>();
      	private static Map<String, Campaign> campaignResultByNameMap = new Map<String, Campaign>();
	  	private static Map<Id, Profile> profileResultByUserIdMap = new Map<Id, Profile>();
	  	private static Map<id,Map<id,User>> userResultByIdMap = new  Map<id,Map<id,User>>();
		
		// Cache Profile SOQL result by Id
		public static Profile getProfileByUserId(Id id)
		 {
		    // Do we already have the result in cache? 
		    Profile resultProfile = profileResultByUserIdMap.get(id);
		    if (resultProfile == null) {
		    	//Not in cache, query the database
		    	 Profile queryResultProfile = [SELECT id, name FROM Profile WHERE id = :id];
		         profileResultByUserIdMap.put(id,queryResultProfile);
		         return queryResultProfile;
		    } else {
		        // In cache, return our cached result immediately!
		       	return resultProfile;
		    }
   
		 }
		 
	 	// Cache Campaign SOQL result by CampaignName
		public static Campaign getCampaignByName(String campaignName)
		 {
		    // Do we already have the result in cache? 
		    Campaign resultCampaign = campaignResultByNameMap.get(campaignName);
		    if (resultCampaign == null) {
		    	//Not in cache, query the database
		    	 Campaign queryResultCampaign = [Select id,name from Campaign where name = :campaignName];
		         campaignResultByNameMap.put(campaignName, queryResultCampaign);
		         return queryResultCampaign;
		    } else {
		       	// In cache, return our cached result immediately!
		       	return resultCampaign;
		    }
   
		 }
		 
		// Cache Profile SOQL result by ProfileName
		public static Profile getProfileByName(String profileName)
		 {
		    // Do we already have the result in cache? 
		    Profile resultProfile = profileResultByNameMap.get(profileName);
		    if (resultProfile == null) {
		 		//Not in cache, query the database
		 		 Profile queryResultProfile = [SELECT id, name FROM Profile WHERE Name = :profileName];
		         profileResultByNameMap.put(profileName, queryResultProfile);
		         return queryResultProfile;
		    } else {
		       // In cache, return our cached result immediately!
		       return resultProfile;
		    }
   
		 }
		 
		 // Cache Map<Id,User> result by AccountId
		 public static Map<id,User> getUserByListIds(List <Id> parAccOwnerIdList)
		 {
		 	Id accId = null;
		 	//If List has a single accountId
		 	if(parAccOwnerIdList.size()==1){
		 		accId = parAccOwnerIdList.get(0);
		    	// Do we already have the result in cache? 
		 		Map<id,User> cachedMap = userResultByIdMap.get(accId);
		     	if (cachedMap == null) {
		    		//Not in cache, query the database
		 	 		 Map<id,User> queryMap = new Map<id,user>([select id, name, profile.name from user where id = :accId]);
			         userResultByIdMap.put(accId, queryMap);
			         return queryMap;
			    } else {
		       	   // In cache, return our cached result immediately!
			       return cachedMap;
			    }
		 	}
		 		//Query the database 
		 		Map<id,User> fullQueryMap = new Map<id,user>([select id, name, profile.name from user where id in :parAccOwnerIdList]);
				return fullQueryMap;
		 }
		 
			 
}