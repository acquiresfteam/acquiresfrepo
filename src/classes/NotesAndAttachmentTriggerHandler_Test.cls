@isTest
public class NotesAndAttachmentTriggerHandler_Test {
    
    public static testMethod void CountAttachementsOnLead_Test(){
        Lead ld = new Lead();
        ld.lastName = 'Test';
        ld.Status = 'New';
        insert ld;
        
        Test.startTest();
        
        Attachment attach1=new Attachment();
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1 =Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        attach1.parentId=ld.id;
        insert attach1;
        
        
        Attachment attach2=new Attachment();
        attach2.Name='Unit Test Attachment';
        Blob bodyBlob2 =Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob2;
        attach2.parentId=ld.id;
        insert attach2; 
        
        delete attach1;
        delete attach2;
                
        Test.stopTest();
    }

    public static testMethod void CountNotesOnLead_Test(){
        Lead ld = new Lead();
        ld.lastName = 'Test';
        ld.Status = 'New';
        insert ld;
        
        Test.startTest();
        
        Note Note1=new Note();
        Note1.Body='Test Body';
        Note1.Title='Test Title';
        Note1.parentId=ld.id;
        insert Note1;
        
        
        Note Note2=new Note();
        Note2.Body='Test Body';
        Note2.Title='Test Title';
        Note2.parentId=ld.id;
        insert Note2;
        
        delete Note1;
        delete Note2;
                
        Test.stopTest();
    }
}