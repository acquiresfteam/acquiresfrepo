public with sharing class TaskTriggerHandler{

    // This method counts number of times No answer Btn is clicked and
    // Counts number of No Answer tasks created for Lead and Account
    public static void countOnNoAnswer(Map<Id, Task> taskNewMap){
        Set<Id> LeadIDs = new Set<ID> () ;
        Set<Id> accIDs = new Set<ID> ();
        
        for( Task objTask : taskNewMap.values() ) {
            if(objTask.Subject == 'No Answer'){
                LeadIDs.add(objTask.whoid );
                accIDs.add(objTask.whatid);
            }
        }

        if( LeadIDs != null && LeadIDs.size() > 0 ){
            // Below trigger is used to count number of No Answer tasks on Lead
            List<Lead> leadList = [ Select Id ,LeadSource , RecordType.Name,Count__c   from Lead where ID IN : LeadIDs] ; 
            if(leadList != null && leadList.size() > 0){
                List<Lead> updateLeadList = new  List<Lead>();
                for(Lead lead : leadList){
                    if(String.ValueOf(lead.LeadSource)!= null && 
                        (String.ValueOf(lead.LeadSource).Contains('Inbound') || lead.LeadSource == 'Carlton FC' || 
                        lead.RecordType.Name == 'Employer')){ 
                        for(Task taskObj : taskNewMap.values()){
                            if(lead.id == taskObj.whoid){
                                if(lead.Count__c == Null){    
                                    lead.Count__c = 1;
                                } 
                                else {
                                    lead.Count__c++;
                                }
                            }
                        }
                        updateLeadList.add(lead);
                    }
                }
                try{
                    update updateLeadList;
                }catch(Exception ex){
                    System.debug('Exception occured on Lead updation::-'+ex);
                }
            }
        }
                
        if( accIDs != null && accIDs.size() > 0 ){            
            // Below trigger is used to count number of No Answer tasks on Account
            List<Account> accList = [ Select id, Count__c from Account where id IN : accIDs] ;
            if(accList != null && accList.size() > 0){
                List<Account> updateAccList = new  List<Account>();
                for(Account acc: accList){
                    for(Task taskObj : taskNewMap.values()){
                        if(acc.id == taskObj.whatid){
                            if(acc.Count__c == Null){    
                                acc.Count__c = 1;
                            } 
                            else {
                                acc.Count__c++;
                            }
                        }
                    }
                    updateAccList.add(acc);
                }
                
                try{
                    update updateAccList;
                }catch(Exception ex){
                    System.debug('Exception occured on Account updation::-'+ex);
                }               
            }
        }
    }
}