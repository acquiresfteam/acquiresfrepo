/************************
    * Class:        CreateNewLeadfromEmailTest
    * Author:       Michael Cooksley (Cloud Sherpas)
    * Description:  Test class for Lead Triggers.
    ************************/
    
@isTest
private class LeadUtilityTest {

    static testMethod void firstTest() {
    
        Test.startTest();
        // Create Queues.
        // Create User.
    
        // Setup data in Meta rating table.
        List<Meta_Rating__c> insertMetaRating = new List<Meta_Rating__c>();
        Meta_Rating__c mR1 = new Meta_Rating__c(Age_Lower__c = 18, Age_Upper__c = 45, Category__c = 'General', Education__c = 'Diploma', Gender__c = 'Male', Lead_Source__c = 'Recruit Easy', Rating__c = '1A');
        insertMetaRating.add(mR1);
        Meta_Rating__c mR2 = new Meta_Rating__c(Age_Lower__c = 18, Age_Upper__c = 45, Category__c = 'General', Education__c = 'Diploma', Gender__c = 'Female', Lead_Source__c = 'Recruit Easy', Rating__c = '1B');
        insertMetaRating.add(mR2);
        insert insertMetaRating;
        
        // Setup data in Meta lead source table.    
        List<Meta_Lead_Source__c> insertMetaLeadSource = new List<Meta_Lead_Source__c>();
        Meta_Lead_Source__c mLS1 = new Meta_Lead_Source__c(Category__c = 'General', Domain__c = 'recruiteasy.com.au', Lead_Source__c = 'Recruit Easy', Queue_Name__c = 'PH Recruit Easy Unassigned', Type__c = 'Queue');
        insertMetaLeadSource.add(mLS1);
        Meta_Lead_Source__c mLS2 = new Meta_Lead_Source__c(Category__c = 'IT', Domain__c = 'recruiteasy.com.au', Lead_Source__c = 'Recruit Easy', Queue_Name__c = 'PH Recruit Easy IT Unassigned', Type__c = 'Queue');
        insertMetaLeadSource.add(mLS2);      
        insert insertMetaLeadSource;
        
        Date doB = Date.parse('01/01/1986');
        
        // Insert a new lead with status 'Qualified for Contact'
        Id careerAdviserRecordTypeId= Lead.SObjectType.getDescribe().getRecordTypeInfosByName().get('Career Adviser').getRecordTypeId();
        Lead l1 = new Lead();
        l1.FirstName = 'George';
        l1.LastName = 'Robbinson';
        l1.LeadSource = 'Recruit Easy';
        l1.Category__c = 'General';
        l1.Gender__c = 'Male';
        l1.Highest_Education__c = 'Diploma';
        l1.Date_Of_Birth__c = doB;
        l1.RecordTypeId = careerAdviserRecordTypeId;
        l1.Status = 'Qualified for Contact';
        l1.Phone = '0404894920';
        insert l1;
        
        // Insert a new lead with status 'New'
        Lead l2 = new Lead();
        l2.FirstName = 'Sally';
        l2.LastName = 'Robbinson';
        l2.LeadSource = 'Recruit Easy';
        l2.Category__c = 'General';
        l2.Gender__c = 'Female';
        l2.Highest_Education__c = 'Diploma';
        l2.Date_Of_Birth__c = doB;
        l2.RecordTypeId = careerAdviserRecordTypeId;
        l2.Status = 'New';
        l2.Phone = '0404894920';
        insert l2;
        
        // Update the lead to 'Qualified for Contact'  
        l2.Status = 'Qualified for Contact';
        update l2;
        
        Test.stopTest();  
    }
    
    public static testMethod void UpdateEmpolyerLeadRating_Test(){
        RecordType recType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName =: 'Employer' AND SobjectType =: 'Lead'];
        
        Test.startTest();
        
        Employer_Meta_Rating__c emplRating = new Employer_Meta_Rating__c();
        emplRating.name = 'E-001';
        emplRating.Classification__c = 'Administration & Office Support';
        emplRating.Sub_Classification__c = 'Client & Sales Administration';
        emplRating.Job_Location__c = 'Sydney';
        emplRating.Job_Location_State__c = 'New South Wales';
        emplRating.Area_Specific__c = 'North West & Hills District';
        emplRating.Classification_Rating__c = '1B';
        emplRating.Location_Rating__c = '2A';
        insert emplRating;
                
        Lead lead = new Lead();
        lead.recordtypeid = recType.id;
        lead.FirstName = 'George';
        lead.LastName = 'Robbinson';
        lead.Phone = '0404894920';
        lead.Lead_Source_for_Employer__c = 'Indeed';
        lead.Classification__c = 'Administration & Office Support';
        lead.Sub_Classification__c = 'Client & Sales Administration';
        lead.Job_Title__c = 'Consultant';
        lead.Work_Type__c = 'Full Time';
        lead.Job_Location__c = 'Sydney';
        lead.Job_Location_State__c = 'New South Wales';
        lead.Area_Specific__c = 'North West & Hills District';
        insert lead;

        
        Test.stopTest(); 
    }    
}