public class AccountTrgUtil{

    public static Boolean flag = false;
    
    public Static void updateAccountOwner(Map<Id, Account> oldAccMap, Map<Id, Account> newAccMap){
        try{
            List<Id> accOwnerIds = new List<Id>();
            for(Account acc: newAccMap.values()){
                accOwnerIds.add(acc.ownerid);
            }
            
            //Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id in :accOwnerIds]);
            Map<id,user> mpuser = QueryUtility.getUserByListIds(accOwnerIds);
            
            Profile profile = QueryUtility.getProfileByUserId(userInfo.getProfileId());
            //Profile profile = [Select id, name From Profile Where id=: userInfo.getProfileId()]; 
            
            for(Account newAcc: newAccMap.Values()){
                Account oldAcc = oldAccMap.get(newAcc.id); 

                String newOwnerName = (mpuser.get(newAccMap.get(newAcc.id).ownerid).name).trim();
                String newOwnerProfile = (mpuser.get(newAccMap.get(newAcc.id).ownerid).profile.name).trim();

                if(oldAcc.ownerId != newAcc.ownerId &&
                    (profile.name == 'Acquire Career Champion' || profile.name == 'Eddi Edvisor') &&
                    (newOwnerProfile == 'Acquire Career Champion' || newOwnerProfile == 'Eddi Edvisor')){
                        newAcc.Previous_record_ownerName__c = newOwnerName;
                }
                
                if(oldAcc.id==newAcc.id &&
                    newAcc.Opportunity_Stage__c == 'Course in Progress' &&
                    profile.name == 'Acquire Retention Specialist' ){
                        newAcc.Retension_Specialist_ID__c = oldAcc.OwnerId;
                        newAcc.Retension_Specialist_Name__c = userInfo.getName();
                }

                if(newAcc.Opportunity_Stage__c == 'Course in Progress' &&
                 (newAcc.Previous_record_ownerId__c != null && newAcc.Previous_record_ownerId__c!= '')){
                     newAcc.OwnerId = newAcc.Previous_record_ownerId__c;  
                     //acc.Previous_record_ownerName__c = acc.Owner.Name;                                 
                }
            }
        }catch(exception ex){
            system.debug('ex ############:' + ex);
        }
    }
        
    public static void nextActivityUpdate(Map<Id, Account> accMap){
        try{
           List<Account> accs = accMap.Values();
            for(Account acc: accs ){
                 if(acc.RSP_Stage_1_Complete__c == false){
                     if( acc.X01_Welcome_Call_And_Email__c  == null || acc.X01_Welcome_Call_And_Email__c  == '0' ){
                         acc.Next_Activity__c = '01 Welcome Call and Email';
                     }else if(acc.X02_Induction_Call__c == null || acc.X02_Induction_Call__c == '0'){
                         acc.Next_Activity__c = '02 Induction Call';
                     }
                     else if(acc.X03_Goal_Setting__c == null || acc.X03_Goal_Setting__c == '0'){
                        acc.Next_Activity__c = '03 Goal Setting';
                     }
                     else if(acc.X04_Personality_Profiling__c == null || acc.X04_Personality_Profiling__c == '0'){
                        acc.Next_Activity__c = '04 Personality Profiling';
                     }
                     else if(acc.X05_Social_Media_Strategy__c == null || acc.X05_Social_Media_Strategy__c == '0'){
                         acc.Next_Activity__c = '05 Social Media Strategy';
                     }
                 }else if(acc.RSP_Stage_2_Complete__c == false){
                  if( acc.X06_LinkedIn_Resume_Cover_Letter__c  == null || acc.X06_LinkedIn_Resume_Cover_Letter__c  == '0' ){
                         acc.Next_Activity__c = '06 LinkedIn Resume/Cover Letter';
                     }else if(acc.X07_Interview_Techniques_And_Tips__c == null || acc.X07_Interview_Techniques_And_Tips__c == '0'){
                         acc.Next_Activity__c = '07 Interview Techniques & Tips';
                     }
                     else if(acc.X08_Dress_for_Success__c == null || acc.X08_Dress_for_Success__c == '0'){
                        acc.Next_Activity__c = '08 Dress for Success';
                     }
                     else if(acc.X09_Job_Search_Techniques__c == null || acc.X09_Job_Search_Techniques__c == '0'){
                        acc.Next_Activity__c = '09 Job Search Techniques';
                     }
                 }else if(acc.RSP_Stage_3_Complete__c == false) {
                 if( acc.X10_Hidden_Job_Approach__c  == null || acc.X10_Hidden_Job_Approach__c  == '0' ){
                         acc.Next_Activity__c = '10 Hidden Job Approach';
                     }
                     else if(acc.X11_Keeping_a_Job__c == null || acc.X11_Keeping_a_Job__c == '0'){
                        acc.Next_Activity__c = '11 Keeping a Job';
                     }
                     else if(acc.X12_Internships_or_Acquire_Experiences__c == null || acc.X12_Internships_or_Acquire_Experiences__c == '0'){
                        acc.Next_Activity__c = '12 Internships or Acquire Experiences';
                     }
                     else if(acc.X13_Role_Play_for_Interview__c == null || acc.X13_Role_Play_for_Interview__c == '0'){
                         acc.Next_Activity__c = '13 Role Play for Interview';
                     }
                 }else {
                     acc.Next_Activity__c = 'No Activity';
                 }
            } 
        }catch(exception ex){
            system.debug('ex ############:' + ex);
        }
    }
    
    public static void updateOppStage(Map<Id, Account> accMap){
        if(!flag){
            try{
                List<Account> accs = accMap.Values();
                
                List<Opportunity> oppList = 
                    [Select id, name, ownerId, StageName, accountId,Cancelation_Reasons__c,Source_of_Cancelation__c
                    From Opportunity Where accountId IN: accMap.keySet()];
                
                //Profile profile = [Select id, name From Profile Where id=: userInfo.getProfileId()];
                Profile profile = QueryUtility.getProfileByUserId(userInfo.getProfileId());
                
                List<Opportunity> oppToUpdate = new List<Opportunity>();    
                //for(Account acc: accs){
                    for(Opportunity opp: oppList){
                        Account acc = accMap.get(opp.AccountId);
                        if(acc.Recent_Closed_Opportunity_Id__c != null && 
                            acc.Recent_Closed_Opportunity_Id__c != '' &&
                            opp.id == acc.Recent_Closed_Opportunity_Id__c){
                                Boolean isUpdate = false;
                                if(acc.Opportunity_Stage__c == 'Course Deffered Confirmed'){
                                    opp.stagename = 'Course Deffered Confirmed';
                                    isUpdate = true;
                                } else if(acc.Opportunity_Stage__c == 'Cancellation confirmed'){
                                    opp.stagename = 'Cancellation confirmed';
                                    isUpdate = true;
                                } else if(acc.Opportunity_Stage__c == 'Cancellation Requested'){
                                    opp.StageName = 'Cancellation Requested';
                                    opp.Cancelation_Reasons__c = acc.Opportunity_Cancellation_Reason__c;
                                    opp.Source_of_Cancelation__c = acc.Source_of_Cancelation__c;
                                    isUpdate = true;
                                } else if(acc.Opportunity_Stage__c == 'Course Deffered Requested'){
                                    opp.stageName = 'Course Deffered Requested';
                                    isUpdate = true;
                                }else if(acc.Opportunity_Stage__c == 'Course in Progress'){
                                    opp.Cancelation_Reasons__c = '';
                                    opp.Source_of_Cancelation__c = '';
                                    isUpdate = true;
                                }
                                if((profile.name == 'Acquire Retention Specialist' || profile.name == 'Acquire Sales Admin - Retention') &&
                                    acc.Opportunity_Stage__c == 'Course in Progress'){
                                        opp.StageName = 'Enrolled - Confirmed';
                                        isUpdate = true;
                                }

                                if(isUpdate) {
                                    oppToUpdate.add(opp);
                                }
                        }
                    }
                //}
                flag  = true;
                update oppToUpdate;
            }catch(exception ex){
                system.debug('ex ############:' + ex);
                flag = false;
            }
        } else {
            flag = false;
        }    
    }
    
     public static void createCampaignMember(Map<Id, Account> newAccMap){
        try{
         List<Id> accOwnerIds = new List<Id>();
                for(Account acc: newAccMap.values()){
                    accOwnerIds.add(acc.ownerid);
                }
                //Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id in :accOwnerIds]);
                   Map<id,user> mpuser = QueryUtility.getUserByListIds(accOwnerIds);
         
                //Campaign Cmpn = [Select id,name from Campaign where name =: 'Student Welcome Email'];
                Campaign Cmpn = QueryUtility.getCampaignByName('Student Welcome Email');
                List<CampaignMember> updateCmpgnMemList = new List<CampaignMember>();

                for(Account newAcc: newAccMap.Values()){
                    String newOwnerProfile = (mpuser.get(newAccMap.get(newAcc.id).ownerid).profile.name).trim();
                    System.debug('$$$$$'+newAcc.Edu_Prov_Rising_Star_Prog_Participant__c);
                    
                   if(newOwnerProfile == 'Acquire Career Champion' && newAcc.Is_campaign_member_created__c == false && 
                     newAcc.Edu_Prov_Rising_Star_Prog_Participant__c == true){
                        newAcc.Is_campaign_member_created__c = true;
                        CampaignMember campMem = new CampaignMember();
                        campMem.CampaignId = Cmpn.Id;
                        campMem.ContactId = newAcc.PersonContactId;
                        campMem.Education_Provider_Name_for_Emails__c = newAcc.Education_Provider_Name_for_Emails__c;
                        campMem.Status = 'Sent';
                        updateCmpgnMemList.add(campMem);
                     
                    }
                }
                insert updateCmpgnMemList; 
                
        }
        catch(exception ex){
            system.debug('ex ############:' + ex);
        }
    }
    
    public static void createVentureCampaignMember(List<Account> newAcc , Map<Id,Account> OldAccMap){
        try{
         //List<Id> acctOwnerIds = new List<Id>();
               // for(Account acc: newAcctMap.values()){
                //    acctOwnerIds.add(acc.ownerid);
               // }
               // Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id in :acctOwnerIds]);
                
                //Campaign Campn = [Select id,name from Campaign where name =: 'Venture Email'];
				Campaign Campn = QueryUtility.getCampaignByName('Venture Email');
                
                List<CampaignMember> updateVentureCmpgnMemList = new List<CampaignMember>();
                
               // Account NewAcct = new Account();
                for(Account acc:newAcc )
                {
              	   Account oldAcc = OldAccMap.get(acc.id);
                 
                   if(acc.Opportunity_Stage__c != oldAcc.Opportunity_Stage__c && acc.Opportunity_Stage__c == 'Course in Progress' && acc.Is_Venture_Campaign_Member_Created__c== false 
                       && acc.Venture_Participant__c == true && acc.Education_Provider_Name__c=='Careers Australia' ){
                        acc.Is_Venture_Campaign_Member_Created__c= true;
                        CampaignMember campMem = new CampaignMember();
                        campMem.CampaignId = Campn.Id;
                        campMem.ContactId = acc.PersonContactId;
                        campMem.Education_Provider_Name_for_Emails__c = acc.Education_Provider_Name_for_Emails__c;
                        campMem.Status = 'Sent';
                        updateVentureCmpgnMemList.add(campMem);
                  
                    }
                   
                }
               /* NewAcct=[select id,name,Education_Provider_Account__c ,PersonContactId from Account where Name = 'Careers Australia'];
                //for(Account newAcc: NewAcct){
                   // String newOwnerProfile = (mpuser.get(newAcctMap.get(newAcc.id).ownerid).profile.name).trim();
                    //System.debug('$$$$$'+newAcc.Edu_Prov_Rising_Star_Prog_Participant__c);
                    
                   if(NewAcct.Opportunity_Stage__c == 'Course in Progress'&& NewAcct.Education_Provider_Account__c == NewAcct.id  && NewAcct.Venture_Participant__c == true){
                        //newAcc.Is_campaign_member_created__c = true;
                        CampaignMember campMem = new CampaignMember();
                        campMem.CampaignId = Cmpn.Id;
                        campMem.ContactId = NewAcct.PersonContactId;
                        campMem.Education_Provider_Name__c = NewAcct.Education_Provider_Name__c;
                        campMem.Status = 'Sent';
                        updateVentureCmpgnMemList.add(campMem);
                     
                    }*/
                //}
                insert updateVentureCmpgnMemList; 
                
        }
        catch(exception ex){
            system.debug('ex ############:' + ex);
        }
    }

    public static void updateAcc(Map<Id, Opportunity> oppMap){
        if(!flag){
            try{
                List<Opportunity> oppList = oppMap.Values();

                List<Id> accIds = new List<Id>();
                for(Opportunity opp: oppList){
                    accIds.add(opp.accountId);
                }
                
                List<Account> accList = 
                [Select id, Education_Provider__c, Course__c, Recent_Closed_Opportunity_Id__c,  Opportunity_Stage__c
                From Account Where id IN: accIds];

              Profile profile = QueryUtility.getProfileByUserId(userInfo.getProfileId());
            //Profile profile = [Select id, name From Profile Where id=: userInfo.getProfileId()];
             
                List<Account> accToUpdate = new List<Account>();
                for(Opportunity opp: oppList){
                    if(opp.StageName == 'Enrolled - Forms Completed' || opp.StageName == 'Enrolled - Confirmed'){
                        system.debug('Applying opportunity ' + opp.Id + ' course to account');
                        for(Account acc: accList){
                            if(opp.accountId == acc.id){
                                acc.Recent_Closed_Opportunity_Id__c = opp.id;
                                acc.Education_Provider_Account__c = opp.Education_Provider_Account__c;
                                acc.Course_LookUp__c = opp.Course_LookUp__c;
                                //acc.Opportunity_Stage__c = 'Course in Progress';
                                accToUpdate.add(acc);
                            }
                        }
                    }
                    
                    if(opp.Stagename == 'Refer to Customer Service'){
                        for(Account acc: accList){
                            if(opp.accountId == acc.id){
                                if(acc.Recent_Closed_Opportunity_Id__c != null && 
                                    acc.Recent_Closed_Opportunity_Id__c != '' &&
                                    opp.id == acc.Recent_Closed_Opportunity_Id__c ){ 
                                    acc.Opportunity_Stage__c = opp.StageName;
                                    accToUpdate.add(acc);
                                }
                            }
                        }
                    }
                    
                    // commented out because of active opportunity functionality.
                    /*if (opp.Stagename == 'Contacted - Info Sent' || opp.StageName == 'Enrolled - Forms Completed') {
                        for (Account acc : accList) {
                            if (opp.accountId == acc.id) {
                                acc.Opportunity_Stage__c = '';
                                accToUpdate.add(acc);
                            }
                        }
                    }*/
                    
                    if((profile.name == 'Acquire Career Advisor' || profile.name == 'Acquire Sales Admin - Retention' || profile.name == 'Acquire Retention Specialist')  && 
                    (opp.StageName == 'Cancellation Requested' || opp.StageName == 'Course Deffered Requested' )){
                        for(Account acc: accList){
                            if(opp.accountId == acc.id){
                                if(acc.Recent_Closed_Opportunity_Id__c != null && 
                                    acc.Recent_Closed_Opportunity_Id__c != '' &&
                                    opp.id == acc.Recent_Closed_Opportunity_Id__c ){ 
                                    acc.Opportunity_Stage__c  = opp.StageName ;
                                    acc.Opportunity_Cancellation_Reason__c = opp.Cancelation_Reasons__c;
                                    acc.Source_of_Cancelation__c = opp.Source_of_Cancelation__c;
                                    accToUpdate.add(acc);
                                }
                            }
                        }
                    }
                }
                flag = true;
                system.debug('Updating accounts');
                update accToUpdate;
            }catch(Exception ex){
                System.debug('UpdateAcc ex ############:' + ex);
                flag = false;
            }
        } else {
            flag = false;
        }

    }
    
    public static void updateOppOwner(Map<Id, Opportunity> oldOppMap, Map<Id, Opportunity> newOppMap){
        try{
             Profile profile = QueryUtility.getProfileByUserId(userInfo.getProfileId());
            //Profile profile = [Select id, name From Profile Where id=: userInfo.getProfileId()];
        
            List<Opportunity> newOppList = newOppMap.values();
            for(Opportunity newOpp: newOppList){
                Opportunity oldOpp = oldOppMap.get(newOpp.id);
                if((profile.name == 'Acquire Career Qualifiers' || profile.name == 'Acquire Recruitment Sales Admin') &&
                (newOpp.StageName == 'Job Shortlisted' || newOpp.StageName == 'Job Reshortlisted') && 
                (newOpp.Career_Hunter_Owner_ID__c != null && newOpp.Career_Hunter_Owner_ID__c != '')){
                    newOpp.OwnerId = oldOpp.Career_Hunter_Owner_ID__c;
                }
                    
                if((profile.name == 'Acquire Retention Specialist' || profile.name == 'Acquire Sales Admin - Retention') &&
                (newOpp.StageName == 'Contacted - Info Sent' || newOpp.StageName == 'Enrolled - Forms Completed') && 
                (newOpp.Career_Advisor_ID__c != null && newOpp.Career_Advisor_ID__c != '')){
                    newOpp.OwnerId = oldOpp.Career_Advisor_ID__c;
                }
            }
        }catch(Exception ex){
            System.debug('ex ############:'+ ex);
        }
    }
}