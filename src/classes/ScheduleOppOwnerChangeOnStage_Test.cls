@isTest
public class ScheduleOppOwnerChangeOnStage_Test{

public static testmethod void UnitTest(){
test.startTest();

RecordType rt = [select id,Name from RecordType where SobjectType='Opportunity' and Name='Employer' Limit 1];

Opportunity Oppt = new Opportunity();
Oppt.RecordTypeId = rt.id;
Oppt.StageName='Contacted - Info Sent';
Oppt.CloseDate = system.today();
Oppt.Name ='sample';
insert Oppt;

Opportunity__c OppAge= new Opportunity__c();
OppAge.Opportunity_Age_Count__c = 1;
insert OppAge;
//Profile profilecamp = [SELECT Id, Name FROM Profile WHERE Name=: 'Acquire Career Champion'];
Profile profilecamp = QueryUtility.getProfileByName('Acquire Career Champion');

User userdetails = new User(Alias = 'standt1', Email='test.user1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US',ProfileId = profilecamp.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        insert userdetails;

ScheduleOppOwnerChangeOnStage newtest = new ScheduleOppOwnerChangeOnStage();
        test.stopTest();

}
}