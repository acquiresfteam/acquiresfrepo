/*
Test class for LeadConvertTrigger : 
a. Create Lead_Attachment__c object for each Attachment on Account object after Lead converstion
b. Create an attachment for the Lead_Attachment__c created
c. Delete the Attachment from Account object
@author: SMendu 
@CreatedDate: 29/04/2015
*/
@isTest
public class LeadConvertTrigger_Test {
	
	private static testMethod void accessLeadAttchFromChampionProfile() {
		  Profile profile = QueryUtility.getProfileByName('Acquire Career Champion');
		  
		  User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        	insert user;
        	
     
        	System.runAs(user) {
     
          		Schema.DescribeSObjectResult drSObj = Schema.sObjectType.Lead_Attachment__c;
          		System.assertEquals (false, drSObj.isCreateable());
          		System.assertEquals (false, drSObj.isAccessible());
          		System.assertEquals (false, drSObj.isUpdateable());
          		System.assertEquals (false, drSObj.isDeletable());
   		}
	}
	
	private static testMethod void createNConvertLead() {
	
		// create a Lead
		Lead lead1=new Lead(LastName='Doe',FirstName='John',
		Status='Qualified for Contact',Category__c='abc',Date_Of_Birth__c=Date.today(), Highest_Level_of_Education__c='MBA',
		Gender__c='M',Highest_Education__c='BE',LeadSource='Acquire careers');
	
		insert lead1;   
		lead1.OwnerId = UserInfo.getUserId();
        update lead1;       
 		
		Attachment newAttch = new Attachment();
		newAttch.Body = Blob.valueOf('some attachment body');
		newAttch.Name = String.valueOf('test.txt');
 		newAttch.ParentId = lead1.Id;
		
	    insert newAttch;
	      
	 	Database.Leadconvert lc = new Database.Leadconvert();
        lc.setLeadId(lead1.id);
        LeadStatus convertStatus = [select Id,MasterLabel from LeadStatus where isConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);  
        lc.setOwnerId(UserInfo.getUserId());
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Test.startTest();
		//Test to check for the creation of Lead_Attachment__c object
		Lead_Attachment__c leadAttchQueRes = [select id,FileName__c,AttachmentId__c,AcctParentId__c  from Lead_Attachment__c where AcctParentId__c = :lcr.getAccountId()];
		System.assertEquals (lcr.getAccountId(), leadAttchQueRes.AcctParentId__c);
		
		//Test to check for an Attachment created on the Lead_Attachment__c object
		List<Attachment> attchResultListOfLA = [Select Id, parentId, body,name,description from Attachment where ParentId = :leadAttchQueRes.Id];
		System.assertEquals (attchResultListOfLA[0].parentId, leadAttchQueRes.Id);
		
		//Test to check the Attachment is deleted on Account
		List<Attachment> attchResultListOfAcc = [Select Id, parentId, body,name,description from Attachment where ParentId = :lcr.getAccountId()];
		System.assertEquals (0, attchResultListOfAcc.size());
			Test.stopTest();
		
	}
	
	
}