@istest
public class accOwnerWelcomeCallUpdate_test 
{
    public static testMethod void accOwnerWelcomeCallUpdateTest() 
    {
       // Profile profile = [SELECT Id FROM Profile WHERE Name='Acquire Career Champion']; 
            Profile profile = QueryUtility.getProfileByName('Acquire Career Champion');
        
        User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = profile.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        insert user;
        
        RecordType personacc = [select id,Name from RecordType where SobjectType='Account' and Name='Person Account' Limit 1];

		Account Acc = new Account();
		Acc.RecordTypeId = personacc.id;
		Acc.LastName='Test';
		insert Acc;

	    RecordType evrt = [select id,Name from RecordType where SobjectType='Event' and Name='Welcome Call Event' Limit 1];

        Event ev = new Event();
        ev.RecordTypeId = evrt.id;
        ev.Subject = 'Welcome Call Booking';
        ev.WhatId=Acc.Id;
        ev.DurationInMinutes=5;
        ev.ActivityDateTime=datetime.now().AddDays(1);
        insert ev;
        
        Account createdAcc = [Select Id,LastName,OwnerId From Account Where Id = :Acc.Id];
        createdAcc.OwnerId = user.Id;
        update createdAcc;
        
        Account createdAcc2 = [Select Id,LastName,OwnerId From Account Where Id = :CreatedAcc.Id];
        Event createdEv = [Select Id,OwnerId From Event Where Id = :ev.Id];
        
        System.debug('Account Owner' + createdAcc2.OwnerId + ' matches Event owner: ' + createdEv.OwnerId);
        system.assertEquals (createdAcc2.OwnerId,createdEv.OwnerId);
    }

}