@isTest
public class ScheduleActivityonLeadage_Test{

public static testmethod void UnitTest(){

Lead Ld = new Lead();
Ld.Status='New';
Ld.LastName='test';
Ld.Phone='9999999999';
Ld.LeadSource ='Spot Jobs - Inbound';

 insert Ld ;
 
 Lead__c leadage= new Lead__c();
 leadage.Lead_Age__c = 2;
  insert leadage;      

 
 Group grp= new Group();
 grp.Name='Inbound Lead Age >7 days';
 grp.Type='Queue';
 insert grp;
   Test.startTest();
   
     
        ScheduleActivityonLeadage newtest = new ScheduleActivityonLeadage();
        String schedule = '0 0 23 * * ?';
        system.schedule('Nightly Update', schedule, newtest );
    test.stopTest();

  }      

}