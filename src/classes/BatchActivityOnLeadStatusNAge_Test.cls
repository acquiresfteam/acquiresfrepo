/*
Test class to test BatchActivityOnLeadStatusNAge for the following
a. Clear Notes__c field
b. Status = 'Qualified for Contact'
c. Owner  =  Queue Id of Queue 'AU Outbound Unable to Contact'
d. Clear Activity History(Tasks) list
e. Clear Date_Unable_to_Call__c field
@author: SMendu 
@CreatedDate: 16/04/2015
*/
@isTest
public class BatchActivityOnLeadStatusNAge_Test{
	
		//To test Lead's with Status=Unable to call & Tasks
		public static testmethod void testLeadUnableToCallWithTask(){
		 	QueueSobject unableToContactQ = [SELECT Id,queue.Name,SobjectType,QueueId  FROM QueueSobject WHERE SobjectType = 'Lead'and queue.Name = 'AU Outbound Unable to Contact'];
    	
			// Create Custom Setting 
			LeadStatusNAge__c customSetting = new LeadStatusNAge__c();
			customSetting.Name = 'Configuration';
			customSetting.LeadAge__c = 2;
			customSetting.LeadStatus__c = 'Unable to call';
			insert customSetting;
	
			RecordType rt = [select id,Name from RecordType where SobjectType='Lead' and Name='Sales Admin' Limit 1];
			   
			Profile profile = QueryUtility.getProfileByName('Acquire Career Champion');
			User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
			            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
			            LocaleSidKey='en_US', ProfileId = profile.Id, 
			            TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
			insert user;
			
			//Create Lead
			Lead lead = new Lead();
			lead.RecordTypeId = rt.id;
			lead.Status='Unable to call';
			lead.notes__C ='Notes 12345';
			lead.LastName='test';
			lead.Phone='9999999999';
			lead.Gender__c ='m';
			lead.Date_Of_Birth__c = Date.today();
			lead.Highest_Education__c ='abc';
			lead.FirstName ='firstname';
			lead.Category__c='category';
			lead.Company ='comp1';
			lead.OwnerId = user.Id;
			lead.LeadSource ='Acquire Inbound Phone';
			insert lead ;
			//Create Task and associate with the Lead
		
			List<Task> Tasks = new List<Task>();
			Tasks.add(new Task(
			ActivityDate = Date.today().addDays(1),
			WhoId = lead.id,
			Status = 'Not Started',
			type='Other',
			Priority='Normal',
			Subject='No Answer'
			));
			insert Tasks;
			
			//Update the Date_Unable_to_Call__c to any date(Setting status to Unable to call will automatically set the Date_Unable_to_Call__c to today, so setting it explicitly)
			lead.Date_Unable_to_Call__c = Date.today().addDays(-3);
			update lead;
			
			Lead lead1 = new Lead();
			lead1.RecordTypeId = rt.id;
			lead1.Status='Unable to call';
			lead1.notes__C ='Notes 12345';
			lead1.LastName='test';
			lead1.Phone='9999999999';
			lead1.Gender__c ='m';
			lead1.Date_Of_Birth__c = Date.today();
			lead1.Highest_Education__c ='abc';
			lead1.FirstName ='firstname';
			lead1.Category__c='category';
			lead1.Company ='comp1';
			lead1.OwnerId = user.Id;
			lead1.LeadSource ='Acquire Inbound Phone';
			insert lead1;
		
			List<Task> Tasks1 = new List<Task>();
			Tasks1.add(new Task(
			ActivityDate = Date.today().addDays(1),
			WhoId = lead1.id,
			Status = 'Not Started',
			type='Other',
			Priority='Normal',
			Subject='No Answer'
			));
			insert Tasks1;
			
			//Update the Date_Unable_to_Call__c to any date(Setting status to Unable to call will automatically set the Date_Unable_to_Call__c to today, so setting it explicitly)
			lead1.Date_Unable_to_Call__c = Date.today().addDays(-3);
			update lead1;
		
			Lead leadQry  = [Select Notes__c, Status, Owner.Id,Date_Unable_to_Call__c,(Select Id, Subject from Tasks)  from LEAD where Id =:lead.id];
			System.debug('Before invoking the Batchable::'+ leadQry.Notes__C+'::'+leadQry.OwnerId +'::'+leadQry.Date_Unable_to_Call__c+'::'+ leadQry.Tasks);
			
			Test.startTest();
			Id batchInstanceId = Database.executeBatch(new BatchActivityOnLeadStatusNAge());
			Test.stopTest();
			Lead leadQryAfterInvoke = [Select Notes__c, Status, Owner.Id,Date_Unable_to_Call__c,(Select Id, Subject from Tasks)  from LEAD where Id =:lead.id];
		    System.assertEquals (null, leadQryAfterInvoke.Notes__c);
		    System.assertEquals ('Qualified for Contact',leadQryAfterInvoke.Status);
		    System.assertEquals (leadQryAfterInvoke.OwnerId,unableToContactQ.QueueId);
		    System.assertEquals (0,leadQryAfterInvoke.tasks.size());
		    System.assertEquals (null,leadQryAfterInvoke.Date_Unable_to_Call__c);
		    
			Lead leadQryAfterInvoke1 = [Select Notes__c, Status, Owner.Id,Date_Unable_to_Call__c,(Select Id, Subject from Tasks)  from LEAD where Id =:lead1.id];

 			System.assertEquals (null, leadQryAfterInvoke1.Notes__c);
		    System.assertEquals ('Qualified for Contact',leadQryAfterInvoke1.Status);
		    System.assertEquals (leadQryAfterInvoke1.OwnerId,unableToContactQ.QueueId);
		    System.assertEquals (0,leadQryAfterInvoke1.tasks.size());
		    System.assertEquals (null,leadQryAfterInvoke1.Date_Unable_to_Call__c);
		  
			System.debug('After invoking the Batchable::'+ leadQryAfterInvoke.Notes__C+'::'+leadQryAfterInvoke.OwnerId +'::'+leadQryAfterInvoke.Date_Unable_to_Call__c+'::'+ leadQryAfterInvoke.Tasks);
		}      
		
		
		//To test Lead's with Status=Unable to call but without Tasks
		public static testmethod void testLeadUnableToCallWithoutTask(){
		 	QueueSobject unableToContactQ = [SELECT Id,queue.Name,SobjectType,QueueId  FROM QueueSobject WHERE SobjectType = 'Lead'and queue.Name = 'AU Outbound Unable to Contact'];
    	
			// Create Custom Setting 
			LeadStatusNAge__c customSetting = new LeadStatusNAge__c();
			customSetting.Name = 'Configuration';
			customSetting.LeadAge__c = 2;
			customSetting.LeadStatus__c = 'Unable to call';
			insert customSetting;
	
			RecordType rt = [select id,Name from RecordType where SobjectType='Lead' and Name='Sales Admin' Limit 1];
			   
			Profile profile = QueryUtility.getProfileByName('Acquire Career Champion');
			User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
			            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
			            LocaleSidKey='en_US', ProfileId = profile.Id, 
			            TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
			insert user;
			
			//Create Lead
			Lead lead = new Lead();
			lead.RecordTypeId = rt.id;
			lead.Status='Unable to call';
			lead.notes__C ='Notes 12345';
			lead.LastName='test';
			lead.Phone='9999999999';
			lead.Gender__c ='m';
			lead.Date_Of_Birth__c = Date.today();
			lead.Highest_Education__c ='abc';
			lead.FirstName ='firstname';
			lead.Category__c='category';
			lead.Company ='comp1';
			lead.OwnerId = user.Id;
			lead.LeadSource ='Acquire Inbound Phone';
			insert lead ;
			
			//Update the Date_Unable_to_Call__c to any date(Setting status to Unable to call will automatically set the Date_Unable_to_Call__c to today, so setting it explicitly)
			lead.Date_Unable_to_Call__c = Date.today().addDays(-3);
			update lead;
			
			Lead leadQry = [Select Notes__c, Status, Owner.Id,Date_Unable_to_Call__c,(Select Id, Subject from Tasks)  from LEAD where Id =:lead.id];
			System.debug('Before invoking the Batchable::'+ leadQry.Notes__C+'::'+leadQry.OwnerId +'::'+leadQry.Date_Unable_to_Call__c+'::'+ leadQry.Tasks);
			
			Test.startTest();
			Id batchInstanceId = Database.executeBatch(new BatchActivityOnLeadStatusNAge());
			Test.stopTest();
			Lead leadQryAfterInvoke = [Select Notes__c, Status, Owner.Id,Date_Unable_to_Call__c,(Select Id, Subject from Tasks)  from LEAD where Id =:lead.id];
		 	System.assertEquals (null, leadQryAfterInvoke.Notes__c);
		    System.assertEquals ('Qualified for Contact',leadQryAfterInvoke.Status);
		    System.assertEquals (leadQryAfterInvoke.OwnerId,unableToContactQ.QueueId);
		    System.assertEquals (0,leadQryAfterInvoke.tasks.size());
		    System.assertEquals (null,leadQryAfterInvoke.Date_Unable_to_Call__c);
			    
			System.debug('After invoking the Batchable::'+ leadQryAfterInvoke.Notes__C+'::'+leadQryAfterInvoke.OwnerId +'::'+leadQryAfterInvoke.Date_Unable_to_Call__c+'::'+ leadQryAfterInvoke.Tasks);
		}      
	
		//Test to see that the code does not fail without the Leads
		public static testmethod void testWithoutLead(){
			// Create Custom Setting 
			LeadStatusNAge__c customSetting = new LeadStatusNAge__c();
			customSetting.Name = 'Configuration';
			customSetting.LeadAge__c = 2;
			customSetting.LeadStatus__c = 'Unable to call';
			insert customSetting;
		
			Test.startTest();
			Id batchInstanceId = Database.executeBatch(new BatchActivityOnLeadStatusNAge());
			Test.stopTest();
		}
		

}