@isTest(seeAllData = true)
private class leadUtil_Test {

    static testMethod void myTest() {

        Lead l1 = new Lead(FirstName = 'Tim',LastName = 'Contact1',Status = 'Qualified for Contact',Highest_Education__c = 'Year 12', Gender__c = 'Male', Category__c = 'General', LeadSource = 'Career One', Date_Of_Birth__c=Date.today());
        insert l1;
        l1.OwnerId = UserInfo.getUserId();
        update l1;
                        
        String attachmentData = 'StartDate,EndDate,PayrollGroupName,EmpGroupName,' +
            'EmpName,ApprovedBy,Payrate,RegHours,Ot1Hours,Ot2Hours,' +
            'Other1Hours,Other2Hours,Other3Hours,Other4Hours,Other5Hours,' +
            'RegCategoryName,Ot1CategoryName,Ot2CategoryName,' +
            'Other1CategoryName,Other2CategoryName,Other3CategoryName,' +
            'Other4CategoryName,Other5CategoryName,RegCategoryCode,' +
            'Ot1CategoryCode,Ot2CategoryCode,Other1CategoryCode,' +
            'Other2CategoryCode,Other3CategoryCode,Other4CategoryCode,' +
            'Other5CategoryCode\n';         
        Blob body1 = Blob.valueOf(attachmentData);
        
        Attachment attachment = new Attachment(
            body=body1,
            Name='Lead_signatureTest.gif',
            ContentType='image/gif',
            ParentId = l1.Id);
            
            insert attachment;
        
        	system.debug('Lead owner Id:' + l1.OwnerId);
                
        Test.startTest();
            Database.Leadconvert lc = new Database.Leadconvert();
            lc.setLeadId(l1.id);
            LeadStatus convertStatus = [select Id,MasterLabel from LeadStatus where isConverted=true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
        	lc.setOwnerId(UserInfo.getUserId());
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            if (lcr.isSuccess()){
                Account resacc = [SELECT Id,OwnerId from Account where Id=:lcr.getAccountId()];
                system.debug('Successfully converted lead into account ' + resacc.Id + ' owned by ' + resacc.OwnerId);
            } else {
                system.debug('Failed to convert lead');
            }

            //See if our attachment has gone
            String Id = null;
            try {
                Id = [select id from Attachment where ParentId =: lcr.getAccountId() limit 1].ParentId; 
            } catch(Exception ex) {}
            
            system.assertEquals(NULL, Id);
                        
                
    }   
}